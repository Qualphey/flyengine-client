
const WORLD = require("./World.js").default;

/**
 * @class GameObject
 * @arg {Object} model
 * @arg {Object} cfg
 * @arg {World} world
 */
module.exports = class {
  constructor(model, cfg) {
    console.log("CREATE GAME OBJECT:");
    console.log(model);
    console.log(cfg);

    this.model = model || {};

    this.x = cfg.x;
    this.z = cfg.z;
    this.wx = cfg.x/50;
    this.wz = cfg.z/50;
    this.angle = cfg.angle;

    if (model) {
      let object;
      let mesh;
      if (model.animations) {
        console.log("ANIMATIONS", model.animations);
        object = this.object = THREE.SkeletonUtils.clone(model.mesh);
        object.traverse( function ( node ) {
          if ( node.isSkinnedMesh ) {
            node.frustumCulled = false;
          }
        });
        mesh = this.mesh = object.children[0].children[3];
      } else {
        object = this.object = model.mesh.clone();
        mesh = this.mesh = object;
      }
      this.geometry = mesh.geometry;


      if (model.animations) {
        this.animated = true;

        let animations = this.animations = model.animations;
        let mixer = this.mixer = new THREE.AnimationMixer(object);
        this.actions = {};
        for (let a = 0; a < animations.length; a++) {
          let anim = animations[a]
          this.actions[anim.name] = mixer.clipAction(anim)
        }

      }

      console.log("MODEL:",model);
      if (model.model_src.endsWith(".dae")) {
        model.texture.flipY = true;
      } else {
        model.texture.flipY = false;
      }

      this.texture = model.texture;
      
      this.material = mesh.material = new THREE.MeshLambertMaterial({
        map: model.texture,
        color: new THREE.Color( 0xffffff ),
         /* wireframe: true */ 
        skinning: this.animated
      });

      let raycaster = new THREE.Raycaster(new THREE.Vector3(this.wx, 10000, this.wz), new THREE.Vector3( 0, -1, 0 ));
      let intersects = raycaster.intersectObject(WORLD.terrain.mesh); //TODO !!! WORLD TERRAIN ONLY !!!
      if (intersects.length > 0) {
        this.y = intersects[0].point.y;
      }

      object.position.set( this.wx, this.y, this.wz );

      scale_val=1.5; // TODO SHOULD BE CONFIGURED OR SCALED EXTERNALLY
      if (cfg.scale) scale_val *= cfg.scale;
      object.scale.set(scale_val, scale_val, scale_val);

      object.name = this.name+cfg.id;
        
      console.log("ADD `", object.name, "` TO SCENE!");
      if (!cfg.dont_add) {
        WORLD.scene.add(object);
      }
    }
  }

  animate(name, loop) {
    if (this.animated) {
      if (this.actions[name]) {
        if (this.action != this.actions[name]) {
          if (loop) this.actions[name].setLoop(loop);
          if (this.action) this.action.stop();
          this.action = this.actions[name].play();
        }
      } else { 
        console.error(new Error("Unable to animate! No animation action `"+name+"` on model `"+this.model.name+"`"));
      }
    } else {
      console.error(new Error("Unable to animate! Model `"+this.model.name+"` has no animations."));
    }
  }

  /**
   * @method mouse_over
   * @memberof GameObject
   * @arg {Event} e mouse event
   */
  mouse_over(e) {
    var mouse = new THREE.Vector2();
    mouse.x = ( e.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = - ( e.clientY / window.innerHeight ) * 2 + 1;

    var raycaster = new THREE.Raycaster();
    raycaster.setFromCamera( mouse, WORLD.camera );

    var intersects = raycaster.intersectObject( this.mesh );
    if (intersects[0]) {
   /* TODO: MOVE SOMEWHERE ELSE
      if (!this.selected) {
        this.select();
      } else {
        this.deselect();
      }
      */
      return true;
    } else {
      return false;
    }
  }

  screen_coords() {
    let obj = this.object;
    var vector = new THREE.Vector3();

    var widthHalf = 0.5*window.innerWidth;
    var heightHalf = 0.5*window.innerHeight;

    obj.updateMatrixWorld();
    vector.setFromMatrixPosition(obj.matrixWorld);
    vector.project(WORLD.camera);

    vector.x = ( vector.x * widthHalf ) + widthHalf;
    vector.y = - ( vector.y * heightHalf ) + heightHalf;

    return { 
      x: vector.x,
      y: vector.y
    };

  }

  update(delta) {
    if (this.mixer) this.mixer.update(delta);
  }

  destroy() {
    console.log("DESTROY GAME OBJECT", this.object);

     WORLD.scene.remove(this.object);


    this.geometry.removeAttribute('position');
    this.geometry.removeAttribute('uv');
    this.geometry.dispose();
    this.geometry = undefined;

    if (this.materials) {
      for (let m = 0; m < this.materials.length; m++) {
        if (this.materials[m]) this.materials[m].dispose();
      }
      this.materials = undefined;
    }

    if (this.material) {
      this.material.dispose();
      this.material = undefined;
    }
    
    this.texture.dispose();
    this.texture = undefined;

    this.data = undefined;
    this.vertices = undefined;
    this.uvs = undefined;

    WORLD.renderer.dispose();
  }

/**
 * @method GameObject#addEventListener
 * @arg {String} event i.e. `click` `hover` `mousedown` `mouseup`
 * @arg {Function} next callback function to be fired on event
 */
  addEventListener(event, next) {
    WORLD.game_controls.event_listeners[event].push({
      next: next,
      obj: this
    });
  }

  removeEventListener(event, next) {
    for (let l = 0; l < WORLD.game_controls.event_listeners[event].length; l++) {
      let item = WORLD.game_controls.event_listeners[event][l];
      if (item.obj == this && item.next == next) {
        WORLD.game_controls.event_listeners[event].splice(l, 1);
      }
    }
  }

  removeAllEventListeners(event) {
    for (let l = 0; l < WORLD.game_controls.event_listeners[event].length; l++) {
      let item = WORLD.game_controls.event_listeners[event][l];
      if (item.obj == this) {
        WORLD.game_controls.event_listeners[event].splice(l, 1);
      }
    }
  }
}
