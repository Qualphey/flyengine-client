'use strict';

const XHR = require("mellisuga/utils/xhr_async.js");

var Environment = require("./environment.js");
var Territory = require("./territory.js");

let BSON = require("bson");

module.exports = class extends Environment {
  constructor(world, env, cfg, terr_cfg) {
    super(world, env, cfg);
    this.territory_cfg = terr_cfg;
    this.cur_territory = false;
  }

  static async init(world, cfg, terr_cfg) {
    try {
      var env = await XHR.get_bson("res/environment/environment.bson");
      return new module.exports(world, env, cfg, terr_cfg);
    } catch (e) {
      console.error(e);
      return undefined;
    }
  }

  display() {
    for (var t = 0; t < this.types.length; t++) {
      this.world.scene.add(this.types[t].mesh);
    }

    console.log("DESTROY");
    this.cur_territory.destroy();
        console.log("DESTROYED");
    console.log(this.cur_territory);
    this.cur_territory = null;
  }

  async display_territory(x, y) {
    for (var t = 0; t < this.types.length; t++) {
      this.world.scene.remove(this.types[t].mesh);
    }

    if (this.cur_territory) {
      this.cur_territory.destroy();
      this.cur_territory = null;
    }

    console.log("SET CUR ENV");
    this.cur_territory = await Territory.init(
      "tile-"+x+"_"+y+".bson",
      this.world,
      this.territory_cfg
    );
  }

  destroy() {
    super.destroy();
    if (this.cur_territory) this.cur_territory.destroy();
  }
}
