
const GameObject = require("../GameObject");

const WORLD = require("../World.js").default;

module.exports = class extends GameObject {
  constructor(cfg, world) {
    super(false, cfg, world);
    let object = cfg.object;
    object = object.mesh;

    var terrain = world.terrain;

    let type = this;
    this.name = cfg.name;
/*
    this.addEventListener("click", async function(e) {
      try {
        let player = world.player;
        if (player.selected_units.length > 0 && e.button == 2) {
          for (let u = 0;u < world.player.selected_units.length; u++) {
            let cunit = world.player.selected_units[u];
            let wpoint = world.terrain.terr_to_world(e.point);
            let path = await cunit.find_path(wpoint);
            player.socket.emit("world.ui-command", {
              command: "unit_gather",
              path: path,
              x: wpoint.x,
              y: wpoint.z,
              what: type.name,
              id: cunit.id
            });
          }
        }
      } catch (e) {
        console.error(e.stack);
      }
    });
*/

    let max_radius = 3;

    let this_class = cfg.this_class; // TODO this is actually environment class

    let m = cfg.m
    let age = cfg.age

    let tileSize = cfg.tile_size;

    let fow_vs = cfg.fow_vs;
    let fow_fs = cfg.fow_fs;

    var object_vertices_length = object.geometry.attributes.position.array.length;
    var object_normals_length = object.geometry.attributes.normal.array.length;
    var object_faces_length = object.geometry.attributes.normal.count/3;
    var object_uvs_length = object.geometry.attributes.uv.array.length;

    let tcount = 0;
    let outob = 0;

    for (let i = 0; i < this_class.map.data.length; i++) {
      let item = this_class.map.data[i].json;
      if (item.type == m && (typeof age == "undefined" || age == item.age)) {
        if (!(Math.abs(item.dx) > max_radius || Math.abs(item.dy) > max_radius)) {
          tcount++;
        } else {
          outob++
        }
      } else {
        outob++;
      }

    }

    console.log("OUT OF BOUNDS", outob);
    console.log("TCOUNT", tcount);

    type.geometry = new THREE.BufferGeometry();
    type.vertices = new Float32Array(tcount*object_faces_length*9);
    type.normals = new Float32Array(tcount*object_faces_length*9);
    type.uvs = new Float32Array(tcount*object_faces_length*6);

    var geo_index = 0;





    for (let i = 0; i < this_class.map.data.length; i++) {
      let item = this_class.map.data[i].json;
      if (item.type == m && (typeof age == "undefined" || age == item.age)
        && !(Math.abs(item.dx) > max_radius || Math.abs(item.dy) > max_radius)) {
        let x = item.x, y = item.y;
          var neometry = object.geometry.clone();

          var matrix4 = new THREE.Matrix4();

          var scale = 10/100*tileSize;

          var pos = {
            x: x*tileSize-this_class.map.width/2*tileSize + item.dx*tileSize*10,
            z: y*tileSize-this_class.map.height/2*tileSize + item.dy*tileSize*10
          }

          if (terrain) {
            var raycaster = new THREE.Raycaster(new THREE.Vector3( pos.x, 10000, pos.z ), new THREE.Vector3( 0, -1, 0 ));
            var intersects = raycaster.intersectObject( terrain.mesh );
            pos.y = intersects[0].point.y;
          }

          matrix4.compose(
            new THREE.Vector3(pos.x, pos.y, pos.z),
            new THREE.Quaternion().setFromAxisAngle( new THREE.Vector3( 0, 1, 0 ), 6.28319 * Math.random() ),
            new THREE.Vector3(scale, scale, scale)
          );

          neometry.applyMatrix(matrix4);

          var object_vertices = neometry.attributes.position.array;
          var object_normals = neometry.attributes.normal.array;
          var object_uvs = neometry.attributes.uv.array;

          for (var f = 0; f < object_faces_length; f++) {
            var face = object_normals[f];
            type.vertices[geo_index*object_faces_length*9 + f*9] = object_vertices[f*9];
            type.vertices[geo_index*object_faces_length*9 + f*9 + 1] = object_vertices[f*9 + 1];
            type.vertices[geo_index*object_faces_length*9 + f*9 + 2] = object_vertices[f*9 + 2];

            type.vertices[geo_index*object_faces_length*9 + f*9 + 3] = object_vertices[f*9 + 3];
            type.vertices[geo_index*object_faces_length*9 + f*9 + 4] = object_vertices[f*9 + 4];
            type.vertices[geo_index*object_faces_length*9 + f*9 + 5] = object_vertices[f*9 + 5];

            type.vertices[geo_index*object_faces_length*9 + f*9 + 6] = object_vertices[f*9 + 6];
            type.vertices[geo_index*object_faces_length*9 + f*9 + 7] = object_vertices[f*9 + 7];
            type.vertices[geo_index*object_faces_length*9 + f*9 + 8] = object_vertices[f*9 + 8];


            type.normals[geo_index*object_faces_length*9 + f*9] = object_normals[f*9];
            type.normals[geo_index*object_faces_length*9 + f*9 + 1] = object_normals[f*9 + 1];
            type.normals[geo_index*object_faces_length*9 + f*9 + 2] = object_normals[f*9 + 2];

            type.normals[geo_index*object_faces_length*9 + f*9 + 3] = object_normals[f*9 + 3];
            type.normals[geo_index*object_faces_length*9 + f*9 + 4] = object_normals[f*9 + 4];
            type.normals[geo_index*object_faces_length*9 + f*9 + 5] = object_normals[f*9 + 5];

            type.normals[geo_index*object_faces_length*9 + f*9 + 6] = object_normals[f*9 + 6];
            type.normals[geo_index*object_faces_length*9 + f*9 + 7] = object_normals[f*9 + 7];
            type.normals[geo_index*object_faces_length*9 + f*9 + 8] = object_normals[f*9 + 8];


            type.uvs[geo_index*object_faces_length*6 + f*6] = object_uvs[f*6];
            type.uvs[geo_index*object_faces_length*6 + f*6 + 1] = object_uvs[f*6 + 1];

            type.uvs[geo_index*object_faces_length*6 + f*6 + 2] = object_uvs[f*6 + 2];
            type.uvs[geo_index*object_faces_length*6 + f*6 + 3] = object_uvs[f*6 + 3];

            type.uvs[geo_index*object_faces_length*6 + f*6 + 4] = object_uvs[f*6 + 4];
            type.uvs[geo_index*object_faces_length*6 + f*6 + 5] = object_uvs[f*6 + 5];
          }

          geo_index++;

          neometry.dispose();
          neometry = null;

          object_vertices = null;
          object_normals = null;
          object_uvs = null;
      }
    }


    type.geometry.addAttribute( 'position', new THREE.BufferAttribute( type.vertices, 3 ) );
//    type.geometry.addAttribute( 'normal', new THREE.BufferAttribute( type.normals, 3 ) );
    type.geometry.addAttribute( 'uv', new THREE.BufferAttribute( type.uvs, 2 ) );

    type.texture = WORLD.models.list[this.name].texture;
    
    let vision_points = [
      new THREE.Vector2(999999, 999999),
    ]

    type.materials = [];

/*    type.materials.push(new THREE.ShaderMaterial({
      uniforms: {
        u_texture: {type: 't', value: type.texture},
        vision_points: {
          type: 'v2v',
          value: vision_points
        },
        vision_range: { value: 1000 }
      },
      side: THREE.DoubleSide,
      color: 0xFFFFFF,
      transparent: true,
      opacity: 1,
      vertexShader: "#define VPOINTSMAX "+vision_points.length+"\n"+fow_vs,
      fragmentShader: fow_fs
    }));*/

    this.materials.push(new THREE.MeshBasicMaterial({ 
      map: type.texture,
      side: THREE.DoubleSide,
      color: 0xFFFFFF,
      transparent: true,
      opacity: 1,
      alphaTest: 0.5
    }));



    type.mesh = new THREE.Mesh( type.geometry, type.materials[0]);

    type.mesh.geometry.computeFaceNormals();
    type.mesh.geometry.computeVertexNormals();
/*
    if (world.config.graphics.shadows) {
      type.mesh.castShadow = true;
      type.mesh.receiveShadow = true;
    }
*/

    type.mesh.name = 'environment_t'+m+'_'+cfg.name+"-"+cfg.age;
    this.object = type.mesh;
    console.log("ADD ENVIRONMENT "+this.name+" TO THE WORLD", type.mesh);
    world.scene.add( type.mesh );
  }

}
