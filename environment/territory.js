const XHR = require("mellisuga/utils/xhr_async.js");

const Environment = require("./environment.js");

module.exports =  class extends Environment {
  constructor(environment, world, cfg) {
    // whole environment controll
    super(world, environment, cfg);

  }

  static async init(tile_name, world, cfg) {
    try {
      var environment = await XHR.get_bson("res/environment/"+tile_name);
      cfg.name = tile_name;
      return new module.exports(environment, world, cfg);
    } catch (e) {
      console.error(e.stack);
      return undefined
    }
  }
}
