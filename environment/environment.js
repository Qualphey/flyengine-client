'use strict';

const fow_vs = require('../terrain/fow.vs').default;
const fow_fs = require('../terrain/fow.fs').default;

const Type = require("./type.js");

const WORLD = require("../World.js").default;

module.exports = class {
  constructor(environment, cfg) {

    var tileSize = cfg.tile_size;

    var terrain = WORLD.terrain;

    this.world = WORLD;
    this.cfg = cfg;

    this.model_objects = [
      null,
      {
        name: "forest",
        ages: [
          WORLD.models.list['forest0'],
          WORLD.models.list['forest1'],
          WORLD.models.list['forest2'],
          WORLD.models.list['forest3']
        ]
      },
      WORLD.models.list['stones'],
      WORLD.models.list['iron'],
      WORLD.models.list['clay']
    ]

    console.log("MODEL OBJECTS" ,this.model_objects);

    this.types = [];
    var this_class = this;

    this_class.map =  {
      width: cfg.height,
      height: cfg.height,
      count: environment.length,
      data: environment
    }
    for (let m = 0; m < this_class.model_objects.length; m++) {
      //console.log(m, this_class.model_objects[m]);
      let object = this_class.model_objects[m];

      if (object != null) {
        if (object.ages) {
          for (let a = 0; a < object.ages.length; a++) {
            let age = object.ages[a];
            let type = new Type({
              name: age.name,
              object: age,
              this_class: this_class,
              age: a,
              m: m,
              tile_size: tileSize,
              fow_vs: fow_vs,
              fow_fs: fow_fs
            }, WORLD);

            this_class.types.push(type);
          }
        } else {
          let type = new Type({
            name: this_class.model_objects[m].name,
            object: object,
            this_class: this_class,
            m: m,
            tile_size: tileSize,
            fow_vs: fow_vs,
            fow_fs: fow_fs
          }, WORLD);

          this_class.types.push(type);
        }
      }
    }
  }

  updateFOW() {
    let vision_points = this.world.player.vision_points();

    for (let t = 0; t < this.types.length; t++) {
      let type = this.types[t];
      if (!type.materials[vision_points.length]) {
        type.materials[vision_points.length] = new THREE.ShaderMaterial({
          uniforms: {
            u_texture: {type: 't', value: type.texture},
            vision_points: {
              type: 'v2v',
              value: vision_points
            },
            vision_range: { value: 1000 }
          },
          vertexShader: "#define VPOINTSMAX "+vision_points.length+"\n"+fow_vs,
          fragmentShader: fow_fs
        });
        type.mesh.material = type.materials[vision_points.length];
      } else {
        if (type.mesh.material == type.materials[vision_points.length]) {
          type.mesh.material.uniforms.vision_points.value = vision_points;
        } else {
          type.mesh.material = type.materials[vision_points.length]
          type.mesh.material.uniforms.vision_points.value = vision_points;
        }
      }
    }
  }

  hit_env(e) {
    let mouse = new THREE.Vector2();
    mouse.x = ( e.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = - ( e.clientY / window.innerHeight ) * 2 + 1;
    for (let t = 0; t < this.types.length; t++) {
      let raycaster = new THREE.Raycaster();
      raycaster.setFromCamera( mouse, this.world.camera );

      let intersects = raycaster.intersectObject( this.types[t].mesh );
      
      if (intersects[0]) {
        return {
          name: this.types[t].name,
          point: new THREE.Vector3(intersects[0].point.x, intersects[0].point.y, intersects[0].point.z)
        };
      }
    }
  }

  destroy() {

    for (var t = 0; t < this.types.length; t++) {
      WORLD.scene.remove(this.types[t].mesh);

      this.types[t].mesh.geometry.dispose();
      this.types[t].mesh.geometry.removeAttribute('position');
      this.types[t].mesh.geometry.removeAttribute('normals');
      this.types[t].mesh.geometry.removeAttribute('uv');
      this.types[t].mesh.geometry = undefined;

      for (let m = 0; m < this.types[t].materials.length; m++) {
        if (this.types[t].materials[m]) {
          this.types[t].materials[m].dispose();
        }
      }
      this.types[t].materials = undefined;

      this.types[t].geometry.dispose();
      this.types[t].geometry.removeAttribute('position');
      this.types[t].geometry.removeAttribute('normals');
      this.types[t].geometry.removeAttribute('uv');
      this.types[t].geometry = undefined;

      this.types[t].texture.dispose();
      this.types[t].texture = undefined;

      this.types[t].vertices = undefined;
      this.types[t].normals = undefined;
      this.types[t].uvs = undefined;

    };

    this.types[t] = undefined;
    

    for (var m = 0; m < this.model_objects.length; m++) {
      this.model_objects[m] = undefined;
    }

    WORLD.renderer.dispose();
  }
}
