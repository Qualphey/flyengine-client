const GameObject = require("../GameObject.js");
const Inventory = require("../player/units/inventory/index.js");

module.exports = class extends GameObject {
  constructor(model, doc, player) {
    super(model, {
      x: doc.x,
      z: doc.z,
      angle: 0
    }, player.world);

    this.animate("Idle", THREE.LoopPingPong);

    this.player = player;
    let world = this.world = player.world;
    let models = world.models.list
   
    let this_class = this;

    this.gui_block = document.createElement("div");
    this.gui_block.innerHTML = "warrior";

    this.inventory = new Inventory(doc.inventory, this);

    this.name = doc.model;
    this.speed = 25;
    this.id = doc.id;
    this.owned = doc.owned;
    this.doc = doc;

    this.angle = 0;

    if (doc.path) {


      //console.log("DOC", doc);

      this.move(
        doc.path
      );
    }

    let smodel = models["selected"];
    let smesh = this.selected_mesh = smodel.mesh.clone();
    smesh.material = new THREE.MeshLambertMaterial({
      map: smodel.texture,
      transparent: true,
      side: THREE.DoubleSide
    });
    let scale_val = 5;
    smesh.scale.set(scale_val, scale_val, scale_val);

    smesh.geometry.computeFaceNormals();
    smesh.geometry.computeVertexNormals();
  }

  select() {
    this.object.add(this.selected_mesh);
    this.selected = true;

    this.world.player.selected_units.push(this);
  }

  deselect() {
    console.log("DESELECT");
    this.object.remove(this.selected_mesh);
    this.selected = false;

    this.world.player.selected_units.splice(this.world.player.selected_units.indexOf(this), 1);
  }


  command(what, options) {
    switch(what) {
      case 'move':
        if (this.owned) {
          WORLD.socket.emit("worldmod.ui-command", {
            command: "unit_move",
            where: options.where,
            id: this.id
          });
        }
        break;
      default:
        console.log('Unit: invalid command: '+what);
    }
  }

  move(path, when_done) {
/*    start_pos.x /= 50
    start_pos.y /= 50
    dest_pos.x /= 50
    dest_pos.y /= 50i*/
    /*
    this.movement = {
      A : start_pos,
      B : dest_pos,
      segment : new THREE.Vector2(
        dest_pos.x - start_pos.x,
        dest_pos.y - start_pos.y
      ),
      tA : start_time,
      tB : dest_time,
      t : dest_time - start_time
    }*/

    //console.log("PATH", path);

    this.path = path;
    this.animate("Run", THREE.LoopPingPong);

    let this_class = this;

    if (when_done) {
      if (this.action_timeout) clearTimeout(this.action_timeout); 
      this.action_timeout = setTimeout(function() {
        //console.log("WHEN DONE DONE", when_done);
        if (when_done.action == "unit_gather") {
          if (when_done.what == "stones" || when_done.what == "iron") {
            this_class.animate("Mine", THREE.LoopPingPong);
          } else if (when_done.what == "forest") {    
            this_class.animate("Cut", THREE.LoopPingPong);
          }
        } else if (when_done.action == "unit_construct") {
          this_class.animate("Construct", THREE.LoopPingPong);
        } else {
          this_class.animate("Idle", THREE.LoopPingPong);
        }
      }, path.dest.time-path.start.time);
    }
  }

  async find_path(point) {
    try {
      let steps = await this.world.astar.find_path({
        x: this.x, y: this.z
      }, {
        x: point.x, y: point.z
      });
      let path = {
        steps: steps,
        start: {
          x: this.x,
          y: this.z
        },
        dest: {
          x: point.x,
          y: point.z
        }
      }
      //console.log("FINAL PATH", path);
      return path;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  stop() {
    this.movement = false;
  }

  update(delta, structures) {
    if (this.path) {
      let now_time = this.world.clock.time;
      if (now_time > this.path.dest.time) {
        this.x = this.path.dest.x;
        this.z = this.path.dest.y;
        this.path = false;
      } else {
        for (let s = 0; s < this.path.steps.length; s++) {
          let step = this.path.steps[s];
          if (now_time < step.time) {
            let s_time = this.path.start.time;
            let sx = this.path.start.x, sy = this.path.start.y;
            if (s > 0) {
              s_time = this.path.steps[s-1].time;
              sx = this.path.steps[s-1].x;
              sy = this.path.steps[s-1].y;
            }


            var passed_time = now_time - s_time;
            var distance = step.length;

            let total_time =  step.time - s_time;
            var passed_distance = passed_time * distance / total_time;

            let dir_vec = (new THREE.Vector2(
              step.x-sx, step.y-sy 
            )).clampLength(passed_distance, 0);

            this.x = sx+dir_vec.x;
            this.z = sy+dir_vec.y;

            this.angle = Math.atan2(
              step.x - this.x,
              step.y - this.z
            );

            break;
          }
        }
      }
    }


    this.wx = this.x/50;
    this.wz = this.z/50;

    if (this.world.terrain.terropen && this.world.terrain.cur_area[4].mesh) {

      const terra = 50*300;

      let terrX = (this.world.terrain.cur_area[4].tile_x-this.world.terrain.width/2)*300*50;
      let terrZ = (this.world.terrain.cur_area[4].tile_y-this.world.terrain.height/2)*300*50;

      const terr_bbox = {
        minX: terrX,
        minY: terrZ,
        maxX: terrX+terra,
        maxY: terrZ+terra,
        contains: function(cx, cy) {
          if (this.minX < cx && this.minY < cy && cx < this.maxX && cy < this.maxY) {
            return true;
          } else {
            return false
          }
        }
      }
      
      if (terr_bbox.contains(this.x, this.z)) {
        let distX = this.x-terr_bbox.maxX+terra/2, distZ = this.z-terr_bbox.maxY+terra/2;
   //     console.log(distX, distZ);
        var raycaster = new THREE.Raycaster(new THREE.Vector3( distX, 10000, distZ ), new THREE.Vector3( 0, -1, 0 ));
        var intersects = raycaster.intersectObject( this.world.terrain.cur_area[4].mesh );
    //    if (intersects[0]) {
          var distY = intersects[0].point.y;
          this.object.position.set( distX, distY, distZ );
          this.object.rotation.y = this.angle//-1.5708;
    //    }
        if (!this.world.scene.getObjectById(this.object.id)) {
          this.world.scene.add(this.object);
        }
      } else {
        if (this.world.scene.getObjectById(this.object.id)) {
          this.world.scene.remove(this.object);
        }
      }
    } else {
      var raycaster = new THREE.Raycaster(new THREE.Vector3(this.wx, 100000, this.wz), new THREE.Vector3( 0, -1, 0 ));
      var intersects = raycaster.intersectObject( this.world.terrain.mesh );

      if (intersects.length > 0) {
        var distY = intersects[0].point.y;
        this.object.position.set(this.wx, distY, this.wz);
      }

      this.object.rotation.y = this.angle//-1.5708;
    } 

    super.update(delta);
  }
/*
  destroy() {

  }*/
}

module.exports.Controlled = require("./controlled/index.js");
module.exports.SemiAutonomous = require("./auto/index.js");
module.exports.Autonomous = require("./auto/index.js");

