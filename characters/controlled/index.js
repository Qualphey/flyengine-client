
const Character = require("../Character.js");
const Inventory = require("../inventory/index.js");

module.exports = class extends Character {
  static async init(cfg, player) {
    try {
      let model = player.world.models.list[cfg.name] //TODO cfg.name -> cfg.model ?
      if (!model) throw new Error("Model `"+cfg.name+"` does not exist!");
      
      /*
       * TODO:
       * cfg.equipement > for each
       *    load model
       */

      // TODO Y -> Z
      cfg.x = Number(cfg.x);
      cfg.z = Number(cfg.y);
      cfg.dest_x = Number(cfg.dest_x);
      cfg.dest_z = Number(cfg.dest_y);
      // TODO Y -> Z

      
      let this_class = new module.exports(model, cfg, player);

      return this_class;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(model, doc, player) {
    super(model, doc, player)
    this.animate("Idle", THREE.LoopPingPong);

    let this_class = this;
    this.addEventListener("click", function(e) {
      console.log("UNIT CLICKED", e.button);
      if (e.button == 0) this_class.select();
    });

 //   if (models[doc.name]) {
      //
/*      var model = models[doc.name]
      var mesh = THREE.SkeletonUtils.clone(model.mesh);
      console.log("mesh", model.mesh);
      console.log("cloned", mesh);
      let animations = this.animations = model.animations;
      mesh.traverse( function ( node ) {
        if ( node.isSkinnedMesh ) {
          node.frustumCulled = false;
        }
      } );
		  let mixer = this.mixer = new THREE.AnimationMixer( mesh );
      this.actions = {
        idle: mixer.clipAction( animations[0] ),
        run: mixer.clipAction( animations[1] )
      } 
      this.actions.run.setLoop(THREE.LoopPingPong);
      this.actions.idle.setLoop(THREE.LoopPingPong);
      this.action = this.actions.idle.play();

      model.texture.flipY = false;
      mesh.children[0].children[3].material = new THREE.MeshLambertMaterial({
        map: model.texture,
        color: new THREE.Color( 0xffffff ),
        */ /* wireframe: true */ 
      /*  skinning: true
      });

      var sword_model = models["sword"]
      var sword_mesh = THREE.SkeletonUtils.clone(sword_model.mesh);
      
      sword_mesh.traverse( function ( node ) {
        if ( node.isSkinnedMesh ) {
          node.frustumCulled = false;
        }
      } );
		  let sword_mixer = this.sword_mixer = new THREE.AnimationMixer( sword_mesh );
      this.sword_actions = {
        idle: sword_mixer.clipAction( animations[0] ),
        run: sword_mixer.clipAction( animations[1] )
      } 
      this.sword_actions.run.setLoop(THREE.LoopPingPong);
      this.sword_actions.idle.setLoop(THREE.LoopPingPong);
      this.sword_action = this.sword_actions.idle.play();

      sword_model.texture.flipY = false;
      sword_mesh.children[0].children[3].material = new THREE.MeshLambertMaterial({
        map: sword_model.texture,
        color: new THREE.Color( 0xffffff ),
         /* wireframe: true */ 
  /*      skinning: true
      });*/


  //    var matrix4 = new THREE.Matrix4();
/*
      matrix4.compose(
        new THREE.Vector3(0, -12, 0),
        new THREE.Quaternion().setFromAxisAngle( new THREE.Vector3( 1, 0, 0 ), 0),
        new THREE.Vector3(10, 10, 10)
      );

      mesh.children[0].children[3].geometry.applyMatrix(matrix4);
*/
//      mesh.children[0].children[3].geometry.computeFaceNormals();
 //     mesh.children[0].children[3].geometry.computeVertexNormals();


      /*
    } else {
      console.error("Model named "+doc.name+" not found. Edit `config.json` to add new models..");
    }*/
  }


  remove() {
    WORLD.scene.remove(this.pivot);
  }
}
