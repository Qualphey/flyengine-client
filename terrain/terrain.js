const GameObject = require("../GameObject.js");

const fow_vs = require('./fow.vs').default;
const fow_fs = require('./fow.fs').default;

const WORLD = require("../World.js").default;


module.exports = class extends GameObject {
  constructor(terrain, canvas, cfg) {
    super(false, {
      x: 0, y: 0, angle: 0, "id": "terrain_"+cfg.name
    });

    this.tile_size = cfg.tile_size;
    // single terrain instance
   /* 
    this.addEventListener("click", async function(e) {
      try {
        let player = WORLD.player;
        if (WORLD.player.selected_units.length > 0) {
          if (e.button == 2) {
            await WORLD.player.move_units(await WORLD.terrain.mouse_coords(e));
          } else if (e.button == 0) {
            if (player.selected_units.length > 0 && player.construct && WORLD.terrain.terropen) {
              if (player.construct.name == "wall") {
                if (player.construct.layout) {
                  player.construct.grid_tiles();
                  let tconstructs = player.construct.layout_data();
                  for (let t = 0; t < tconstructs.length; t++) {
                    let tconstruct = tconstructs[t];
                    let command_data = {
                      command: "build",
                      what: tconstruct.what,
                      lvl: 0,
                      pos: tconstruct.pos,
                      angle: tconstruct.angle,
                      grid_tiles: tconstruct.grid_tiles
                    }
                    player.socket.emit("world.ui-command", command_data);
                  }
                  player.construct = undefined;
                }
                player.construct.lay_out(e.point);
              } else if (player.construct.angle_input) {
                player.construct.grid_tiles();
                let command_data = {
                  command: "build",
                  what: player.construct.name,
                  lvl: 0,
                  pos: player.construct.pos,
                  angle: player.construct.angle,
                  grid_tiles: player.construct.grid_tiles
                }
                player.socket.emit("world.ui-command", command_data);
                WORLD.scene.remove(player.construct.pivot);
                player.construct = undefined;
              } else {
                player.construct.set_position(await WORLD.terrain.mouse_coords(e));
              }
            } else {
              for (let p = 0; p < WORLD.player.selected_units.length; p++) {
                WORLD.player.selected_units[p].deselect();
              }
            }
          }
        } else if (!WORLD.terrain.terropen) {
          console.log("SELECT TERRITORY");
          await WORLD.terrain.select_territory(e);
          WORLD.structures.prep_geometries();
          WORLD.structures.update();
          WORLD.astar.grid_tested = false;
          WORLD.astar.debug_curterr();
        }                      
      } catch (er) {
        console.error(er.stack);
      }
    });
*/

    //console.log("CONSTRUCT TERRAIN", cfg.name);
    //

    this.name = 'terrain_'+cfg.name;
    this.height_mult = cfg.height_multiplier;
    this.tile_size = cfg.tile_size;

    this.data = terrain.data;
    this.width = terrain.width;
    this.height = terrain.height;

    console.log(this.data);

    var vert_count = this.width*this.height*6;
    this.geometry = new THREE.BufferGeometry();
    this.vertices = new Float32Array(vert_count*3);

    var cur_tile = 0;
    
    let tgrid, tx, ty;
    if (WORLD.terrain && WORLD.terrain.grid && cfg.tile_x) {
      tgrid = WORLD.terrain.grid.grid;
      tx = cfg.tile_x-WORLD.terrain.tx;
      ty = cfg.tile_y-WORLD.terrain.ty;


      //console.log("txy", tx, ty);
    }

    for (let x = 0; x < this.width; x++) {
      for (let y = 0; y < this.height; y++) {
        let t = y*this.width+x;
        let v = cur_tile*18;

        // x coords
        this.vertices[ v ] = (x-this.width/2)*this.tile_size+this.tile_size;
        this.vertices[ v + 3 ] = (x-this.width/2)*this.tile_size+this.tile_size;
        this.vertices[ v + 6 ] = (x-this.width/2)*this.tile_size;
        this.vertices[ v + 9] = (x-this.width/2)*this.tile_size;
        this.vertices[ v + 12 ] = (x-this.width/2)*this.tile_size;
        this.vertices[ v + 15 ] = (x-this.width/2)*this.tile_size+this.tile_size;

        // z coords
        this.vertices[ v + 2 ] = (y-this.height/2)*this.tile_size+this.tile_size;
        this.vertices[ v + 5 ] = (y-this.height/2)*this.tile_size;
        this.vertices[ v + 8 ] = (y-this.height/2)*this.tile_size;
        this.vertices[ v + 11 ] = (y-this.height/2)*this.tile_size;
        this.vertices[ v + 14 ] = (y-this.height/2)*this.tile_size+this.tile_size;
        this.vertices[ v + 17 ] = (y-this.height/2)*this.tile_size+this.tile_size;

        // y coords
        if (x == this.width-1) {
          this.vertices[ v + 7 ] = this.data[ t ]*this.height_mult; // X0 Y0
          this.vertices[ v + 10 ] = this.data[ t ]*this.height_mult; // X0 Y0
          this.vertices[ v + 13  ] = (this.data[ t + this.width ] || this.data[ t ])*this.height_mult; // X0 Y1
          if (tgrid) {
            let rterr = tgrid[2+ty][3+tx].data;            this.vertices[ v + 1 ] = (rterr[t+1] || tgrid[3+ty][3+tx].data[0])*this.height_mult; // X1 Y1
            this.vertices[ v + 4 ] = rterr[t-this.width+1]*this.height_mult; // X1 Y0
            this.vertices[ v + 16 ] = (rterr[t+1] || tgrid[3+ty][3+tx].data[0])*this.height_mult; // X1 Y1
            if (y == this.height-1) {
              let bterr = tgrid[3+ty][2+tx].data;
              this.vertices[ v + 13  ] = bterr[t-this.width*(this.height-1)]*this.height_mult; // X0 Y1
            }
          } else {
            this.vertices[ v + 1 ] = (this.data[ t + this.width ] || this.data[ t ])*this.height_mult; // X1 Y1
            this.vertices[ v + 4 ] = this.data[ t ]*this.height_mult; // X1 Y0
            this.vertices[ v + 16 ] = (this.data[ t + this.width ] || this.data[ t ])*this.height_mult; // X1 Y1
          }
        } else if (y == this.height-1) {
          if (tgrid) {
            let bterr = tgrid[3+ty][2+tx].data;
            this.vertices[ v + 1 ] = bterr[t-this.width*(this.height-1)+1]*this.height_mult; // X1 Y1
            this.vertices[ v + 13  ] = bterr[t-this.width*(this.height-1)]*this.height_mult; // X0 Y1
            this.vertices[ v + 16 ] = bterr[t-this.width*(this.height-1)+1]*this.height_mult; // X1 Y1
          } else {
            this.vertices[ v + 1 ] = (this.data[ t + this.width ] || this.data[ t ])*this.height_mult; // X1 Y1
            this.vertices[ v + 13  ] = (this.data[ t + this.width ] || this.data[ t ])*this.height_mult; // X0 Y1
            this.vertices[ v + 16 ] = (this.data[ t + this.width ] || this.data[ t ])*this.height_mult; // X1 Y1
          }
          this.vertices[ v + 4 ] = this.data[ t+1 ]*this.height_mult; // X1 Y0
          this.vertices[ v + 7 ] = this.data[ t ]*this.height_mult; // X0 Y0
          this.vertices[ v + 10 ] = this.data[ t ]*this.height_mult; // X0 Y0
        } else {
          this.vertices[ v + 1 ] = (this.data[ t + this.width + 1] || this.data[ t + 1] || this.data[ t ])*this.height_mult;
          this.vertices[ v + 4 ] = (this.data[ t+1 ] || this.data[ t - this.width + 1])*this.height_mult;
          this.vertices[ v + 7 ] = this.data[ t ]*this.height_mult;
          this.vertices[ v + 10 ] = this.data[ t ]*this.height_mult;
          this.vertices[ v + 13  ] = (this.data[ t + this.width ] || this.data[ t ])*this.height_mult;
          this.vertices[ v + 16 ] = (this.data[ t + this.width + 1] || this.data[ t + 1] || this.data[ t ])*this.height_mult;
        }

        for (let i = 0; i < 18; i++)  {
          var item = this.vertices[v+i];
          if (isNaN(item)) {
            console.log("NaN", x, y, i);
            console.log(t, this.height_mult, this.width, v+i, this.data[v+i]);
            this.vertices[v+i] = 0;
          }
        }

        cur_tile++;
      }
    }


    this.uvs = new Float32Array(vert_count*2);

    var cur_tile = 0;

    for (let x = 0; x < this.width; x++) {
      for (let y = 0; y < this.height; y++) {
        let t = y*this.width+x;
        let v = cur_tile*12;

        var uv_x = 1/this.width*x, uv_y = 1/this.height*y,
        uv_x_p = uv_x+1/this.width, uv_y_p = uv_y+1/this.height;

        this.uvs[ v ] = uv_x_p;
        this.uvs[ v + 1 ] = uv_y_p;

        this.uvs[ v + 2 ] = uv_x_p;
        this.uvs[ v + 3 ] = uv_y;

        this.uvs[ v + 4 ] = uv_x;
        this.uvs[ v + 5 ] = uv_y;

        this.uvs[ v + 6] = uv_x;
        this.uvs[ v + 7 ] = uv_y;

        this.uvs[ v + 8 ] = uv_x;
        this.uvs[ v + 9 ] = uv_y_p;

        this.uvs[ v + 10 ] = uv_x_p;
        this.uvs[ v + 11 ] = uv_y_p;

        for (let i = v; i < v+18; i++)  {
          var item = this.vertices[i];
          if (isNaN(item)) {
            console.log("NaN", x, y, i);
          }
        }

        cur_tile++;
      }
    }

    this.geometry.addAttribute( 'position', new THREE.BufferAttribute( this.vertices, 3 ) );
    this.geometry.addAttribute( 'uv', new THREE.BufferAttribute( this.uvs, 2 ) );


    this.texture = new THREE.Texture(canvas.element);
    this.texture.needsUpdate = true;

    this.geometry.computeVertexNormals();

    let vision_points = [
      new THREE.Vector2(0, 0),
    ]
    this.materials = [];
    if (cfg.vs) {
      console.log("SET HIDE SHADER");
      this.materials.push(new THREE.ShaderMaterial({
        uniforms: {
          u_texture: {type: 't', value: this.texture},
          hide_range: { value: 1200 },
          fogColor: { type: "c" },
          fogNear: { type: "f" },
          fogFar: { type: "f" }
        },
        vertexShader: cfg.vs,
        fragmentShader: cfg.fs,
        transparent: true,
        alphaTest: 0.5,
        fog: true
      }));
/*
    this.materials.push(new THREE.ShaderMaterial({
      uniforms: {
        u_texture: {type: 't', value: this.texture},
        vision_points: {
          type: 'v2v',
          value: vision_points
        },
        vision_range: { value: 1000 }
      },
      vertexShader: "#define VPOINTSMAX "+vision_points.length+"\n"+fow_vs,
      fragmentShader: fow_fs
    }));*/
    } else {
      this.materials.push(new THREE.MeshBasicMaterial({ map: this.texture }));
    }

    var mesh = this.mesh = new THREE.Mesh( this.geometry, this.materials[0]);
    mesh.geometry.computeFaceNormals();
    mesh.geometry.computeVertexNormals();

    if (typeof cfg.x == 'number' && typeof cfg.y == 'number') {
      mesh.position.set(cfg.x, 0, cfg.y);
    }

/*
    if (WORLD.config.graphics.shadows) {
      mesh.receiveShadow = true;
    }
*/
    mesh.name = this.name;

    console.log("ADD "+mesh.name+" terrain MESH");
    WORLD.scene.add(mesh);

  }

  updateFOW() {
    let vision_points = WORLD.player.vision_points();
//    if (!this.materials) this.materials = [];
    if (!this.materials[vision_points.length]) {
      this.materials[vision_points.length] = new THREE.ShaderMaterial({
        uniforms: {
          u_texture: {type: 't', value: this.texture},
          vision_points: {
            type: 'v2v',
            value: vision_points
          },
          vision_range: { value: 1000 }
        },
        vertexShader: "#define VPOINTSMAX "+vision_points.length+"\n"+fow_vs,
        fragmentShader: fow_fs
      });
      this.mesh.material = this.materials[vision_points.length];
    } else {
      if (this.mesh.material == this.materials[vision_points.length]) {
        this.mesh.material.uniforms.vision_points.value = vision_points;
        this.mesh.material.uniforms.vision_points.needsUpdate = true;
      } else {
        this.mesh.material = this.materials[vision_points.length]
        this.mesh.material.uniforms.vision_points.value = vision_points;
      }
    }
  }

  getHeightAt(x, z) {
    var v0 = this.data[z*this.width+x];
    var v1 = this.data[z*this.width+x+1] || this.data[z*this.width+x];
    var v2 = this.data[z*this.width+this.width+x] || this.data[z*this.width+x];
    var v3 = this.data[z*this.width+this.width+x+1] || this.data[z*this.width+x+1] || this.data[z*this.width+x];
    if (x == this.width-1) {
      return (v0+v2)/2*this.height_mult;
    } else {
      return (v0+v1+v2+v3)/4*this.height_mult;
    }
  }

  getCoords(point) {
    var cursor = new THREE.Vector2(point.x+this.tile_size/2, point.z+this.tile_size/2);
    cursor.x = Math.round(cursor.x / this.tile_size)*this.tile_size;
    cursor.y = Math.round(cursor.y / this.tile_size)*this.tile_size;

    let hw = Math.floor(this.width/2)-1;
    let hh = Math.floor(this.height/2)-1;

    return new THREE.Vector2(cursor.x / this.tile_size + hw, cursor.y / this.tile_size + hh);
  }

  getPlaneHeightsAt(coords) {
    var v0 = this.data[coords.y*this.width+coords.x]*this.height_mult;
    var v1 = this.data[coords.y*this.width+coords.x+1]*this.height_mult;
    var v2 = this.data[coords.y*this.width+this.width+coords.x]*this.height_mult;
    var v3 = this.data[coords.y*this.width+this.width+coords.x+1]*this.height_mult;
    if (coords.x == this.width-1 && coords.y == this.height-1) {
      v1 = this.data[coords.y*this.width+coords.x]*this.height_mult;
      v2 = this.data[coords.y*this.width+coords.x]*this.height_mult;
      v3 = this.data[coords.y*this.width+coords.x]*this.height_mult;
    } else if (coords.x == this.width-1) {
      v1 = this.data[coords.y*this.width+coords.x]*this.height_mult;
      v3 = this.data[coords.y*this.width+this.width+coords.x]*this.height_mult;
    } else if (coords.y == this.height-1) {
      v2 = this.data[coords.y*this.width+coords.x]*this.height_mult;
      v3 = this.data[coords.y*this.width+coords.x+1]*this.height_mult;
    }

    return [v0, v1, v2, v3];
  }


  getHeightAtScene(scene_x, scene_z) {
    var x = Math.round((scene_x + this.width*this.tile_size/2)/this.tile_size);
    var z = Math.round((scene_z + this.height*this.tile_size/2)/this.tile_size);

    var v0 = this.data[z*this.width+x];
    var v1 = this.data[z*this.width+x+1] || this.data[z*this.width+x];
    var v2 = this.data[z*this.width+this.width+x] || this.data[z*this.width+x];
    var v3 = this.data[z*this.width+this.width+x+1] || this.data[z*this.width+x+1] || this.data[z*this.width+x];
    if (x == this.width-1) {
      return (v0+v2)/2*this.height_mult;
    } else {
      return (v0+v1+v2+v3)/4*this.height_mult;
    }
  }

  hide() {
    WORLD.scene.remove(this.mesh);
    WORLD.scene.remove(this.tile_cursor);
    this.hidden = true;
  }

  show() {
    WORLD.scene.add(this.mesh);
    WORLD.scene.add(this.tile_cursor);
    this.hidden = false;
  }

  destroy() {
    console.log("destroy terrain", this.name);

    WORLD.scene.remove(this.mesh);
    this.mesh = null;


    this.geometry.removeAttribute('position');
    this.geometry.removeAttribute('uv');
    this.geometry.dispose();
    this.geometry = undefined;

    for (let m = 0; m < this.materials.length; m++) {
      if (this.materials[m]) this.materials[m].dispose();
    }
    this.materials = undefined;

    this.texture.dispose();
    this.texture = undefined;

    this.data = undefined;
    this.vertices = undefined;
    this.uvs = undefined;

    WORLD.renderer.dispose();
  }
}
