const XHR = require("mellisuga/utils/xhr_async.js");

const Terrain = require("./terrain.js");
const Canvas = require("./canvas.js");
const Territory = require("./territory.js");

let BSON = require("bson");

const TerritoryGrid = require("./TerritoryGrid.js");


const terra = 50*300;
module.exports =  class extends Terrain {
  static async init(res_path, world, cfg, terr_cfg) {
    try {
      var terrain = await XHR.get_bson(res_path+"/world.bson");
      terrain.width = terrain.height = Math.sqrt(terrain.data.buffer.length);
      let environment = {
        width: terrain.width,
        height: terrain.height,
        data: new Uint8Array(terrain.data.buffer.length)
      } 
      console.log("TERRAIN?", terrain);
      var canvas = await Canvas.init(environment, terrain.width, terrain.height);
      return new module.exports({
        width: terrain.width,
        height: terrain.height,
        data: terrain.data.buffer
      }, canvas, world, cfg, terr_cfg, res_path);
    } catch(e) {
      console.error(e.stack);
      return undefined;
    };
  }

  constructor(terrain_cfg, canvas, world, cfg, terr_cfg, res_path) {
    // whole terrain controll
    cfg.name = 'main';
    super(terrain_cfg, canvas, world, cfg);

    this.res_path = res_path;

    this.sub_a = terrain_cfg.sub_a;


    this.world = world;
    this.terr_cfg = terr_cfg;

    this.cur_area = [];
    this.cur_envs = [];
    var this_class = this;

//    this.grid = new TerritoryGrid();

    // TERRITORY CURSOR

    var tc_geometry = new THREE.PlaneGeometry(this.tile_size, this.tile_size);
    var tc_matrix4 = new THREE.Matrix4();
    tc_matrix4.compose(
      new THREE.Vector3(0, 0, 0),
      new THREE.Quaternion().setFromAxisAngle( new THREE.Vector3( 0, 0, 1 ), 1.5708 ),
      new THREE.Vector3(1, 1, 1)
    );

    tc_geometry.applyMatrix(tc_matrix4);

    var tc_material = new THREE.MeshBasicMaterial( { color: 0x3399CC } );
    tc_material.transparent = true;
    tc_material.opacity = 0.4;
    tc_material.needsUpdate = true;

    var territory_cursor = this.terr_cursor = new THREE.Mesh(tc_geometry, tc_material);
    this.terr_cursor.name = "terr_cursor";
    territory_cursor.rotation.x = -1.5708;
    world.scene.add( territory_cursor );

    window.addEventListener("mousemove", function(e) {
      if (world.scene.mouseover && !world.destroyed) {

        var mouse = new THREE.Vector2();
        mouse.x = ( e.clientX / window.innerWidth ) * 2 - 1;
        mouse.y = - ( e.clientY / window.innerHeight ) * 2 + 1;

        var raycaster = new THREE.Raycaster();
        raycaster.setFromCamera( mouse, world.camera );

        var intersects = raycaster.intersectObject( this_class.mesh );

        for (var i = 0; i < intersects.length; i++) {
          var tile_coords = this_class.getCoords(intersects[i].point);
          var heights = this_class.getPlaneHeightsAt(tile_coords);

          var pos_y = this_class.getHeightAt(tile_coords.x, tile_coords.y);
          var cursor = new THREE.Vector3(intersects[i].point.x+this_class.tile_size/2, pos_y, intersects[i].point.z+this_class.tile_size/2);
          cursor.x = Math.round(cursor.x / this_class.tile_size)*this_class.tile_size;
          cursor.z = Math.round(cursor.z / this_class.tile_size)*this_class.tile_size;

          cursor.x -= this_class.tile_size/2;
          cursor.z -= this_class.tile_size/2;

          territory_cursor.position.set(cursor.x, 5, cursor.z);

          territory_cursor.geometry.vertices[ 0 ].z = heights[2];
          territory_cursor.geometry.vertices[ 1 ].z = heights[0];
          territory_cursor.geometry.vertices[ 2 ].z = heights[3];
          territory_cursor.geometry.vertices[ 3 ].z = heights[1];
          territory_cursor.geometry.verticesNeedUpdate = true;

        }
      }
    });
    this.lastHeight = this.getHeightAtScene(0, 0);

    this.back_button = document.createElement("button");
    this.back_button.innerHTML = "<< back to world ";
    this.back_button.classList.add("terrain_back");
    this.back_button.addEventListener('click', function(e) {
      world.load_overlay.show();  
      world.paused = true;
      this_class.terropen = false;
      this_class.clear_grid();

      world.scene.add(this_class.mesh);
      world.environment.display();

      var newHeight = this_class.getHeightAtScene(world.controls.target.x, world.controls.target.z);
      var delta_height = newHeight - this_class.lastHeight;
      world.controls.target.y = newHeight;
      world.camera.position.y = world.camera.position.y + delta_height;

      var cam_at_height = this_class.getHeightAtScene(world.camera.position.x, world.camera.position.z);
      if (cam_at_height+100 > world.camera.position.y) {
        world.camera.position.y = cam_at_height+100;
      }

      this_class.lastHeight = newHeight;

      document.body.removeChild(this_class.back_button);
      this_class.current_territory = false;
      world.structures.update();
      world.paused = false;
      world.load_overlay.hide();  
    });

    world.controls.addEventListener('change', async function() {
      try {
        if (!world.paused) {
          let min_x = this_class.width*this_class.tile_size/2*-1;
          let max_x = this_class.width*this_class.tile_size/2-200;
          let min_z = this_class.height*this_class.tile_size/2*-1;
          let max_z = this_class.height*this_class.tile_size/2-200; // TODO WHY - 200 ??

          if (this_class.terropen) { 
            min_x = 50*this_class.tile_size/2*-1;
            max_x = 50*this_class.tile_size/2;
            min_z = 50*this_class.tile_size/2*-1;
            max_z = 50*this_class.tile_size/2;
          }


          if (this_class.current_territory) {
            var newHeight = this_class.current_territory.getHeightAtScene(world.controls.target.x, world.controls.target.z);
            if (!isNaN(newHeight)) {
              var delta_height = newHeight - this_class.lastHeight;
              world.controls.target.y = newHeight;
              world.camera.position.y = world.camera.position.y + delta_height;

              var cam_at_height = this_class.current_territory.getHeightAtScene(world.camera.position.x, world.camera.position.z);
              if (cam_at_height+100 > world.camera.position.y) {
                world.camera.position.y = cam_at_height+100;
              }


              this_class.lastHeight = newHeight;
            }
          } else {
            var newHeight = this_class.getHeightAtScene(world.controls.target.x, world.controls.target.z);
            if (!isNaN(newHeight)) {
              var delta_height = newHeight - this_class.lastHeight;
              world.controls.target.y = newHeight;
              world.camera.position.y = world.camera.position.y + delta_height;

              var cam_at_height = this_class.getHeightAtScene(world.camera.position.x, world.camera.position.z);
              if (cam_at_height+100 > world.camera.position.y) {
                world.camera.position.y = cam_at_height+100;
              }

              

              this_class.lastHeight = newHeight;
            }
          }
          var prevent = false;

          let sx=0, sy=0;
          if (world.controls.target.x < min_x) {
            if (this_class.terropen) {
              sx--;
            } else {
              world.controls.target.x = min_x;
            }
            prevent = true;
          }
          if (world.controls.target.x > max_x) {
            if (this_class.terropen) {
              sx++;
            } else {
              world.controls.target.x = max_x;
            }
            prevent = true;
          }
          if (world.controls.target.z < min_z) {
            if (this_class.terropen) {
              sy--;
            } else {
              world.controls.target.z = min_z;
            }
            prevent = true;
          }
          if (world.controls.target.z > max_z) {
            if (this_class.terropen) {
              sy++;
            } else {
              world.controls.target.z = max_z;
            }
            prevent = true;
          }

          if (sx != 0 || sy != 0) await this_class.switch_territory(sx, sy);
    //      world.controls.target.x = max_x;
    //      world.controls.target.z = max_z;

    //      console.log("X", min_x, world.controls.target.x, max_x);
    //      console.log("Y", min_z, world.controls.target.z, max_z);

        }
      } catch (e) {
        console.error(e.stack);
      }
    });
  }

  async switch_territory(sx, sy) {
    try {
      this.world.load_overlay.show();
      this.world.paused = true;
      this.clear_grid();
      await this.display_territory(this.tx+sx, this.ty+sy);
      this.current_territory = this.cur_area[4];
      let cam_trans = 300*50-100;
      this.world.controls.target.x -= cam_trans*sx;
      this.world.camera.position.x -= cam_trans*sx;
      this.world.controls.target.z -= cam_trans*sy;
      this.world.camera.position.z -= cam_trans*sy;
      console.log("UNPAUSE");
      this.world.paused = false;
      this.world.load_overlay.hide();
    } catch (e) {
      console.error(e.stack);
    }
  }
  
  terr_coords(tx, ty, x, y) {
    let terrX = tx*terra;
    let terrZ = ty*terra;
    console.log(terrX, terrZ, x, y, tx, ty);

    let terr_bbox = {
      minX: terrX,
      minY: terrZ,
      maxX: terrX+terra,
      maxY: terrZ+terra,
      contains: function(cx, cy) {
        if (this.minX < cx && this.minY < cy && cx < this.maxX && cy < this.maxY) {
          return true;
        } else {
          return false
        }
      }
    }
    
    if (terr_bbox.contains(x, y)) {
      return new THREE.Vector2(x-terr_bbox.maxX+terra/2, y-terr_bbox.maxY+terra/2);
    } else {
      return undefined;
    }
  }

  curterr_coords(x, y) {
    if (this.terropen) {
      let terrX = (this.cur_area[4].tile_x-this.width/2)*terra;
      let terrZ = (this.cur_area[4].tile_y-this.height/2)*terra;

      let terr_bbox = {
        minX: terrX,
        minY: terrZ,
        maxX: terrX+terra,
        maxY: terrZ+terra,
        contains: function(cx, cy) {
          if (this.minX < cx && this.minY < cy && cx < this.maxX && cy < this.maxY) {
            return true;
          } else {
            return false
          }
        }
      }
      
      if (terr_bbox.contains(x, y)) {
        return new THREE.Vector2(x-terr_bbox.maxX+terra/2, y-terr_bbox.maxY+terra/2);
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  curterr_contains(x, y) {

    let terrX = (this.cur_area[4].tile_x-this.width/2)*300*50;
    let terrZ = (this.cur_area[4].tile_y-this.height/2)*300*50;

    let terr_bbox = {
      minX: terrX,
      minY: terrZ,
      maxX: terrX+terra,
      maxY: terrZ+terra,
      contains: function(cx, cy) {
        if (this.minX < cx && this.minY < cy && cx < this.maxX && cy < this.maxY) {
          return true;
        } else {
          return false
        }
      }
    }
    
    return terr_bbox.contains(x, y);
  }

  async mouse_coords(e) {
    try {
      if (this.terropen) {
        return this.terr_to_world(await this.cast_ray(e, this.cur_area[4].mesh));
      } else {
        let point = await this.cast_ray(e, this.mesh)
        return point.multiplyScalar(50);
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  terr_to_world(vec, tx, ty) {
    if (tx != undefined && ty != undefined) {
      let terrX = (tx+0.5)*terra;
      let terrZ = (ty+0.5)*terra;
      return vec.add(new THREE.Vector3(terrX, 0, terrZ)); 
    } else {
      let terrX = (this.world.terrain.cur_area[4].tile_x+0.5-this.world.terrain.width/2)*terra;
      let terrZ = (this.world.terrain.cur_area[4].tile_y+0.5-this.world.terrain.height/2)*terra;
      return vec.add(new THREE.Vector3(terrX, 0, terrZ)); 
    }
  }

  async cast_ray(e) { 
    try {
      let mesh;
      if (this.terropen) {
        mesh = this.cur_area[4].mesh;
      } else {
        mesh = this.mesh;
      }

      var mouse = new THREE.Vector2();
      mouse.x = ( e.clientX / window.innerWidth ) * 2 - 1;
      mouse.y = - ( e.clientY / window.innerHeight ) * 2 + 1;

      var raycaster = new THREE.Raycaster();
      raycaster.setFromCamera( mouse, this.world.camera );

      var intersects = raycaster.intersectObject( mesh );

      if (intersects[0]) {
        return new THREE.Vector3(intersects[0].point.x, intersects[0].point.y, intersects[0].point.z);
      } else {
        return undefined
      }
    } catch(e) {
      console.error(e.stack);
    }
  }

  clear_grid() {
    for (let a = 0; a < this.cur_area.length; a++) {
      this.cur_area[a].destroy();
    }
    this.cur_area = [];
  }

  async display_territory(tx, ty) {
    try {
      this.tx = tx;
      this.ty = ty;
//      await this.grid.preload(tx, ty);
//      this.clear_grid();

      for (let x = tx-1; x < tx+2; x++) {
        for (let y = ty-1; y < ty+2; y++) {
          let tile_cfg = JSON.parse(JSON.stringify(this.terr_cfg));
          tile_cfg.x = (x-tx)*300*50;
          tile_cfg.y = (y-ty)*300*50;
          tile_cfg.tile_x = x;
          tile_cfg.tile_y = y;

          const terr_bson = await XHR.get_bson(this.res_path+"/region-"+x+"_"+y+"/region.bson");
          this.cur_area.push(await Territory.init("/region-"+x+"_"+y+"/", this.world, tile_cfg));
        }
      }
      this.current_territory = this.cur_area[4];
      console.log("NEW CURRENT TERRITORY", this.current_territory);
//          this.current_territory = await Territory.init("tile-"+select_province.x+"_"+select_province.y+".bson", this.world, terr_cfg);
      await this.world.environment.display_territory(tx, ty);
      this.world.structures.update();
      return true;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }
  
  async select_territory(e) {
    try {
      if (this.world.scene.mouseover && e.which == 1 && !this.current_territory) {
        this.world.paused = true;
        let terr_cfg = this.terr_cfg
        this.world.load_overlay.show();

        var mouse = new THREE.Vector2();
        mouse.x = ( e.clientX / window.innerWidth ) * 2 - 1;
        mouse.y = - ( e.clientY / window.innerHeight ) * 2 + 1;

        var raycaster = new THREE.Raycaster();
        raycaster.setFromCamera( mouse, this.world.camera );

        var intersects = raycaster.intersectObject( this.mesh );

        for (var i = 0; i < intersects.length; i++) {
          var cursor = new THREE.Vector3(intersects[i].point.x+this.tile_size/2, intersects[i].point.y, intersects[i].point.z+this.tile_size/2);
          cursor.x = Math.round(cursor.x / this.tile_size)*this.tile_size;
          cursor.z = Math.round(cursor.z / this.tile_size)*this.tile_size;

          let hw = Math.floor(this.width/2)-1;
          let hh = Math.floor(this.height/2)-1;

          var select_province =  new THREE.Vector2(cursor.x / this.tile_size + hw, cursor.z / this.tile_size + hh);

          console.log("tile select", select_province);

          this.world.scene.remove(this.mesh);

          console.log("CUR TERRITORY", this.current_territory); // Synchronize territory destruction



          this.tx = select_province.x;
          this.ty = select_province.y;

          await this.display_territory(this.tx, this.ty);
          this.terropen = true;

          document.body.appendChild(this.back_button);

          var newHeight = this.current_territory.getHeightAtScene(this.world.controls.target.x, this.world.controls.target.z);
          var delta_height = newHeight - this.lastHeight;
          this.world.controls.target.y = newHeight;
          this.world.camera.position.y = this.world.camera.position.y + delta_height;

          var cam_at_height = this.current_territory.getHeightAtScene(this.world.camera.position.x, this.world.camera.position.z);
          if (cam_at_height+100 > this.world.camera.position.y) {
            this.world.camera.position.y = cam_at_height+100;
          }

          this.lastHeight = newHeight;

        }
        this.world.paused = false;
        this.world.load_overlay.hide();
      }
    } catch(e) {
      console.error(e.stack);
    }
  } 

  destroy() {
    super.destroy();
    this.world.scene.remove(this.terr_cursor);
    for (let t = 0; t < this.cur_area.length; t++) {
      if (this.cur_area[t]) {
        this.cur_area[t].destroy();
      }
    }
  }
}
