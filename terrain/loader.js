const XHR = require("mellisuga/utils/xhr_async.js");

module.exports = class {
  static async load(path) {
    try {
      const terrain = await XHR.get_bson(path);
      terrain.width = terrain.height = Math.sqrt(terrain.data.buffer.length);
      terrain.data = terrain.data.buffer;
      if (terrain.rivers) terrain.rivers = terrain.rivers.buffer;
      return terrain;
    } catch (e) {
      console.error(e.stack);
    }
  }
}
