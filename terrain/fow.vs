uniform vec2 vision_points[VPOINTSMAX];
uniform float vision_range;

varying vec2 v_texCoord;
varying float vision;

void main() {
  vision = 1.0;
  float hvr = vision_range/2.0;
  float rvr = vision_range/4.0*3.0;
  for (int v = 0; v < VPOINTSMAX; v++) {
    vec2 pos2 = vec2(position.x, position.z);
    float distance = length(pos2-vision_points[v]);
    if (distance > rvr) {
      vision -= min((distance-rvr)/hvr, 0.5);
    } else {
      vision = 1.0;
      break;
    }
  }
  
  vision = max(vision, 0.5);

  // Pass the texcoord to the fragment shader.
  v_texCoord = uv;

  gl_Position = projectionMatrix *
                modelViewMatrix *
                  vec4(position,1.0);
}
