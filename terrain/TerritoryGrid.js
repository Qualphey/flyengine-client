const XHR = require("mellisuga/utils/xhr_async.js");

module.exports = class {
  static async init() {
    try {
      return new module.exports();
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  constructor() {
    this.paths = {
      terrain: "res/terrain/",
      environment: "res/environment/"
    }
    this.grid = [];
    for (let y = 0; y < 5; y++) {
      this.grid.push(new Array(5));
    }
  }

  async preload(sx, sy) {
    try {
      for (let x = -2; x < 3; x++) {
        for (let y = -2; y < 3; y++) {
          this.grid[y+2][x+2] = await XHR.get_bson(this.paths.terrain+"tile-"+(sx+x)+"_"+(sy+y)+".bson");
        }
      }
      console.log("territory grid", this.grid);
//      debugger;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }
}
