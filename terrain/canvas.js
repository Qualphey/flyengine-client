
var XHR = require('mellisuga/utils/xhr_async.js');
let BSON = require('bson');

const size = 90;
const step = 30;
const pos_fix = (size-step)/2;

module.exports = class {
  constructor(environment, imgs, w, h) {
  //  console.log("CONSTRUCT CANVAS", w, h);
    this.element = document.createElement('canvas');

    let width = w*step, height = h*step;
  //  console.log("CANVAS SIZE", width, height);

    function nextPow2( aSize ){
      return Math.pow( 2, Math.ceil( Math.log( aSize ) / Math.log( 2 ) ) );
    }

    this.element.width = nextPow2(width);
    this.element.height = nextPow2(height);

    this.step = this.element.width/w;
  //  console.log("STEP", this.step);
    this.pos_fix = (size-this.step)/2;

  //  console.log("CANVAS SIZE", width, height);
  //  console.log("CANVAS ELEMENT SIZE", this.element.width, this.element.height);

    // get context
    var context = this.element.getContext( '2d' );

    var canvas_list = [];

    for (var i = 0; i < imgs.length; i++) {
      var ncanv = img_canvas(imgs[i]);
      ncanv.src_rem = imgs[i].src;
      canvas_list.push(ncanv);
    }

    console.log("CANVAS LIST", canvas_list);
    console.log("ENVIRONMENT", environment);

    for (let x = 0; x < environment.width; x++) {
      for (let y = 0; y < environment.height; y++) {
        context.drawImage(canvas_list[0], x*this.step-this.pos_fix, (environment.height-1)*this.step-y*this.step-this.pos_fix);
      }
    }


    for (let x = 0; x < environment.data.length; x++) {
      const env_node = environment.data[x].json;
      for (var i = canvas_list.length-1; i >= 0; i--) {
        if (env_node && env_node.type == i) {
          const nx = environment.width/2 - 10/2 + env_node.x + env_node.dx*10,
            ny = environment.height/2 - 10/2 + env_node.y + env_node.dy*10;
          context.drawImage(
            canvas_list[i],
            nx*this.step- size/2, //- size/2,
            environment.height*this.step-ny*this.step- size/2 
          );
        }
      }
    }


    for (let x = 0; x < environment.width; x++) {
      for (let y = 0; y < environment.height; y++) {
        if (environment.rivers_data[y*environment.height+x] > 0) {
          context.drawImage(canvas_list[5], x*this.step-size/2, environment.height*this.step-y*this.step-size/2);
        }
      }
    }

    context.fillStyle = 'red';
    context.fillRect(0, 0, this.step, this.step);
    context.fillRect(this.element.width-this.step, this.element.height-this.step, this.step, this.step);

    function img_canvas(img) {
      var canvas_img = document.createElement( 'canvas' );
      canvas_img.width = size;
      canvas_img.height = size;

      var img_context = canvas_img.getContext( '2d' );

      img_context.drawImage(img, 0, 0, size, size);
      var imageData = img_context.getImageData(0, 0, size, size);
      var d = imageData.data;
      var maxY = size;

      var cX = size/2;
      var cY = size/2;

      var mexVec = new THREE.Vector2(0-cX, size/2-cY);
      var maxLength = mexVec.length();

      for (var iy, ix, alpha, i = 3, l = d.length; i < l; i += 4) {
        iy = Math.floor(i / 4 / size);
        ix = Math.floor(i / 4 - iy * size);

        var vec = new THREE.Vector2(ix-cX, iy-cY);


        alpha = 255 - Math.round(255 * Math.abs(vec.length()/maxLength));
        d[i] = Math.min(d[i], alpha);
      }

      img_context.clearRect(0, 0, size, size);
      img_context.putImageData(imageData, 0, 0);

      return canvas_img;
    }
  }

  static async init(env_src, w, h) {
    try {
      let environment = env_src;

      //console.log("Environment", environment);

      var img_srcs = [
        "res/grass.png",
        "res/forest_ground.jpg",
        "res/stone.jpg",
        "res/clay.jpg",
        "res/clay.png",
        "res/underwater.png"
      ];

      var imgs = new Array(img_srcs.length);
      for (var i = 0; i < img_srcs.length; i++) {
        //console.log(img_srcs[i]);
        var img = new Image();
        img.src_rem = img_srcs[i];
        img.src = img_srcs[i];
        await new Promise(function(resolve) {
          img.onload = function() {
            resolve();
          };
        });
        imgs[i] = img;
      }

      //console.log("IMGS", imgs);

      return new module.exports(environment, imgs, w, h);
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }
}
