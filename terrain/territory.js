const XHR = require("mellisuga/utils/xhr_async.js");

const Terrain = require("./terrain.js");
const Canvas = require("./canvas.js");


module.exports =  class extends Terrain {
  constructor(terrain_cfg, canvas, world, cfg) {
    // whole terrain controll
    super(terrain_cfg, canvas, world, cfg);
    this.x = cfg.x
    this.y = cfg.y
    this.tile_x = cfg.tile_x
    this.tile_y = cfg.tile_y
  }

  static async init(tile_name, world, cfg) {
    try {
      /*
      let subterrs = new Array(world.terrain.sub_a);
      for (let x = 0; x < world.terrain.sub_a; x++) {
        subterrs[x] = new Array(world.terrain.sub_a);
        for (let y = 0; y < world.terrain.sub_a; y++) {
          subterrs[x][y] = await XHR.get_bson("res/world/"+tile_name+"sub-territory-"+x+"_"+y+".bson");
        }
      }
      let terr_w = subterrs[0][0].width*world.terrain.sub_a;
      let terr_h = subterrs[0][0].height*world.terrain.sub_a;

*/
      console.log("GETBSON", "res/world/"+tile_name);
      let terrain = await XHR.get_bson("res/world/"+tile_name+"region.bson");
      terrain.width = terrain.height = Math.sqrt(terrain.data.buffer.length);

      console.log(terrain);
      let canvas = await Canvas.init({
        width: terrain.width,
        height: terrain.height,
        data: new Uint8Array(terrain.data.buffer.length)
      }, terrain.width, terrain.height);
      cfg.name = tile_name;
      //console.log(cfg);

      return new module.exports({
        width: terrain.width,
        height: terrain.height,
        data: terrain.data.buffer
      }, canvas, world, cfg);
    } catch (e) {
      console.error(e.stack);
    }
  }
}
