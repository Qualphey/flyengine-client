
const TerrainLoader = require("flyengine-client/terrain/loader.js");
const TerrainCanvas = require("flyengine-client/terrain/canvas.js");
const Terrain = require("flyengine-client/terrain/terrain.js");

const Rivers = require("./rivers.js");

const FarTerrain = require("./far");

module.exports = class extends Terrain {
  static async construct(tx, ty, rx, ry, icanvas) {
    try {

      const radius = 3;
      const total_tiles = Math.pow(radius*2+1, 2);

      let adj_terrain = {

      }

      let bi = 0;
      let vertex_buffer = undefined;;
      let river_buffer = undefined;
      for (let x = tx-radius; x < tx+radius+1; x++) {
        for (let y = ty-radius; y < ty+radius+1; y++) {
          let nrzxy = module.exports.adapt_coordinates(rx, ry, x, y, 10);
          let frx = nrzxy.rx, fry = nrzxy.ry, fzx = nrzxy.zx, fzy = nrzxy.zy;
          
          if (frx < 0 || 10 <= frx || fry < 0 || 10 <= fry) {
            continue;
          }


          module.exports.adapt_coordinates(frx, fry, fzx, fzy, 10);
          console.log("res/world/region-"+frx+"_"+fry+"/zone-"+fzx+"_"+fzy+".bson");
          const terrain = await TerrainLoader.load("res/world/region-"+frx+"_"+fry+"/zone-"+fzx+"_"+fzy+".bson");

          adj_terrain.width = terrain.width * (radius * 2 + 1);
          adj_terrain.height = terrain.height * (radius * 2 + 1);

          if (!vertex_buffer) vertex_buffer = new Uint8Array(terrain.data.length*total_tiles);
          if (!river_buffer) river_buffer = new Uint8Array(terrain.data.length*total_tiles);
          for (let i = 0; i < terrain.height; i++) {
            let lx = x - (tx-radius), ly = y - (ty-radius);
            lx *= terrain.width;
            ly *= terrain.height;
            let li = ly * adj_terrain.width + lx;
            let buffer_index = li+i*adj_terrain.width;
            vertex_buffer.set(terrain.data.slice(i*terrain.width, i*terrain.width+terrain.width), buffer_index);
            river_buffer.set(terrain.rivers.slice(i*terrain.width, i*terrain.width+terrain.width), buffer_index);
          }
          bi += terrain.data.length;
        }
      }

      adj_terrain.data = vertex_buffer;
      adj_terrain.rivers = river_buffer;



      var canvas = await TerrainCanvas.init({
        width: adj_terrain.width,
        height: adj_terrain.height,
        data: icanvas.data || new Uint8Array(adj_terrain.data.length),
        rivers_data: river_buffer
      }, adj_terrain.width, adj_terrain.height);


      let zone_terrain = new module.exports(adj_terrain, canvas, {
        name: "zone-"+tx+"_"+ty,
        height_multiplier: 40,
        tile_size: 400
      });

      zone_terrain.rivers = [];

      zone_terrain.rivers.push(new Rivers(adj_terrain, {
        name: "zoned-"+tx+"_"+ty,
        height_multiplier: 40,
        tile_size: 400,
        decrease_height: 60,
        opacity: 0.8
      }));

      zone_terrain.rivers.push(new Rivers(adj_terrain, {
        name: "zoned-"+tx+"_"+ty,
        height_multiplier: 40,
        tile_size: 400,
        decrease_height: 20,
        opacity: 0.6,
        ox: 0.2, py: 0.2
      }));

      zone_terrain.rivers.push(new Rivers(adj_terrain, {
        name: "zone-"+tx+"_"+ty,
        height_multiplier: 40,
        tile_size: 400,
        opacity: 0.3,
        ox: 0.4, py: 0.4
      }));

      zone_terrain.is_zone = true;

      zone_terrain.crx = rx;
      zone_terrain.cry = ry;
      zone_terrain.czx = tx;
      zone_terrain.czy = ty;



      let fart = zone_terrain.fart = await FarTerrain.construct();
      fart.background(rx, ry, tx, ty);
      
      return zone_terrain;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(terrain_cfg, canvas, cfg) {
    super(terrain_cfg, canvas, cfg);

  }

  static adapt_coordinates(rx, ry, zx, zy, region_size) {
    while (zx < 0) {
      rx--;
      zx += region_size;
    }
    while (zx >= region_size) {
      rx++;
      zx -= region_size;
    }
    while (zy < 0) {
      ry--;
      zy += region_size;
    }
    while (zy >= region_size) {
      ry++;
      zy -= region_size;
    }
    return {
      rx: rx,
      ry: ry,
      zx: zx,
      zy: zy
    }
  }

  hide() {
    for (let r = 0; r < this.rivers.length; r++) {
      this.rivers[r].destroy();
    }
    super.hide();
  }

  show() {
    super.show();
  }

  destroy() {
    this.fart.destroy();
    for (let r = 0; r < this.rivers.length; r++) {
      let river = this.rivers[r];
      river.destroy();
    }
    super.destroy();
  }
}
