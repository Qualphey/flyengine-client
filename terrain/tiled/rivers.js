
const WORLD = require("../../World.js").default;

module.exports = class {
  static async construct() {
    try {
      let _this = new module.exports();
      
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(terrain, cfg) {

    this.name = 'river_'+cfg.name;

    this.height_mult = cfg.height_multiplier;
    this.tile_size = cfg.tile_size;

    console.log("rivers", terrain.rivers);
    this.data = terrain.rivers;
    this.width = terrain.width;
    this.height = terrain.height;
    let river_nodes = 0;

    for (let n = 0; n < this.data.length; n++) {
      let node = this.data[n];
      if (node > 0) river_nodes++;
    }
    console.log("NODES", river_nodes);

    var vert_count = river_nodes*6;
    this.geometry = new THREE.BufferGeometry();
    this.vertices = new Float32Array(vert_count*3);
    this.uvs = new Float32Array(vert_count*2);

    let height_dec = cfg.decrease_height || 0;
    console.log("HEIGHT DEC", height_dec);

    if (!cfg.ox) cfg.ox = 0;
    if (!cfg.oy) cfg.oy = 0;

    let cur_tile = 0;
    for (let x = 0; x < this.width; x++) {
      for (let y = 0; y < this.height; y++) {
        let t = y*this.width+x;
        if (this.data[t] > 0) {
          let v = cur_tile*18;

          // x coords
          this.vertices[ v ] = (x-this.width/2)*this.tile_size+this.tile_size-this.tile_size/2; // E
          this.vertices[ v + 3 ] = (x-this.width/2)*this.tile_size+this.tile_size-this.tile_size/2; // E
          this.vertices[ v + 6 ] = (x-this.width/2)*this.tile_size-this.tile_size/2; // W
          this.vertices[ v + 9] = (x-this.width/2)*this.tile_size-this.tile_size/2; // W
          this.vertices[ v + 12 ] = (x-this.width/2)*this.tile_size-this.tile_size/2; // W
          this.vertices[ v + 15 ] = (x-this.width/2)*this.tile_size+this.tile_size-this.tile_size/2; // E

          // z coords
          this.vertices[ v + 2 ] = (y-this.height/2)*this.tile_size+this.tile_size-this.tile_size/2; // S
          this.vertices[ v + 5 ] = (y-this.height/2)*this.tile_size-this.tile_size/2; // N
          this.vertices[ v + 8 ] = (y-this.height/2)*this.tile_size-this.tile_size/2; // N
          this.vertices[ v + 11 ] = (y-this.height/2)*this.tile_size-this.tile_size/2; // N 
          this.vertices[ v + 14 ] = (y-this.height/2)*this.tile_size+this.tile_size-this.tile_size/2; // S
          this.vertices[ v + 17 ] = (y-this.height/2)*this.tile_size+this.tile_size-this.tile_size/2; // S

          //y coords
          this.vertices[ v + 1 ] = this.data[ t ]*this.height_mult-height_dec; //SE
          this.vertices[ v + 4 ] = this.data[ t ]*this.height_mult-height_dec; //NE
          this.vertices[ v + 7 ] = this.data[ t ]*this.height_mult-height_dec; //NW
          this.vertices[ v + 10 ] = this.data[ t ]*this.height_mult-height_dec; //NW
          this.vertices[ v + 13  ] = this.data[ t ]*this.height_mult-height_dec; //SW
          this.vertices[ v + 16 ] = this.data[ t ]*this.height_mult-height_dec; //SE

          if (this.data[t+1] > 0) { // EAST
            this.vertices[ v + 1 ] = this.data[ t+1 ]*this.height_mult-height_dec; //SE
            this.vertices[ v + 4 ] = this.data[ t+1 ]*this.height_mult-height_dec; //NE
            this.vertices[ v + 16 ] = this.data[ t+1 ]*this.height_mult-height_dec; //SE
          }
          if (this.data[t+this.width] > 0) { // SOUTH
            this.vertices[ v + 1 ] = this.data[ t+this.width ]*this.height_mult-height_dec; //SE
            this.vertices[ v + 13  ] = this.data[ t+this.width ]*this.height_mult-height_dec; //SW
            this.vertices[ v + 16 ] = this.data[ t+this.width ]*this.height_mult-height_dec; //SE
          }

          if ((this.data[t+1] > 0 || this.data[t+this.width]) > 0 && this.data[t+this.width+1] > 0) {
            this.vertices[ v + 16 ] = this.data[ t+this.width+1]*this.height_mult-height_dec; //SE
            this.vertices[ v + 1 ] = this.data[ t+this.width+1 ]*this.height_mult-height_dec; //SE
          }

          if (this.data[t+this.width-1] > 0
            && this.data[t+this.width] == 0) {
            this.vertices[ v + 13  ] = this.data[ t+this.width-1]*this.height_mult-height_dec; //SW
          }


          v = cur_tile*12;

          var uv_x = 0, uv_y = 0,
          uv_x_p = 1, uv_y_p = 1;

          uv_x -= cfg.ox;
          uv_y -= cfg.oy;
          uv_x_p -= cfg.ox;
          uv_y_p -= cfg.oy;


          this.uvs[ v ] = uv_x_p;
          this.uvs[ v + 1 ] = uv_y_p;

          this.uvs[ v + 2 ] = uv_x_p;
          this.uvs[ v + 3 ] = uv_y;

          this.uvs[ v + 4 ] = uv_x;
          this.uvs[ v + 5 ] = uv_y;

          this.uvs[ v + 6] = uv_x;
          this.uvs[ v + 7 ] = uv_y;

          this.uvs[ v + 8 ] = uv_x;
          this.uvs[ v + 9 ] = uv_y_p;

          this.uvs[ v + 10 ] = uv_x_p;
          this.uvs[ v + 11 ] = uv_y_p;



          cur_tile++;
        }
      }
    }



    console.log(this.vertices);
    this.geometry.addAttribute( 'position', new THREE.BufferAttribute( this.vertices, 3 ) );
    this.geometry.addAttribute( 'uv', new THREE.BufferAttribute( this.uvs, 2 ) );


    this.geometry.computeVertexNormals();



    let vision_points = [
      new THREE.Vector2(999999, 999999),
    ] ;
    this.materials = [];


    WORLD.textures.water.wrapS = WORLD.textures.water.wrapT = THREE.RepeatWrapping;
    WORLD.textures.water.needsUpdate = true;

    this.materials.push(new THREE.MeshBasicMaterial({ 
      map: WORLD.textures.water,
      side: THREE.FrontSide,
  //    color: cfg.color || 0x9999FF,
      transparent: true,
      opacity: cfg.opacity || 0.5
    }));

    var mesh = this.mesh = new THREE.Mesh( this.geometry, this.materials[0]);
    mesh.geometry.computeFaceNormals();
    mesh.geometry.computeVertexNormals();


/*
    if (WORLD.config.graphics.shadows) {
      mesh.receiveShadow = true;
    }
*/
    mesh.name = this.name;

    console.log("ADD "+mesh.name+" river MESH", mesh);
    WORLD.scene.add(mesh);
  }

  destroy() {
    console.log("destroy river", this.name);

    WORLD.scene.remove(this.mesh);
    this.mesh = null;


    this.geometry.removeAttribute('position');
    this.geometry.removeAttribute('uv');
    this.geometry.dispose();
    this.geometry = undefined;

    for (let m = 0; m < this.materials.length; m++) {
      if (this.materials[m]) this.materials[m].dispose();
    }
    this.materials = undefined;

    this.data = undefined;
    this.vertices = undefined;

    WORLD.renderer.dispose();
  }
}
