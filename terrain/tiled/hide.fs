
uniform sampler2D u_texture;
varying vec2 v_texCoord;
varying float fog_down;

uniform vec3 fogColor;
uniform float fogNear;
uniform float fogFar;

void main() {
  gl_FragColor = texture2D(u_texture, v_texCoord); 

  #ifdef USE_FOG
    #ifdef USE_LOGDEPTHBUF_EXT
      float depth = gl_FragDepthEXT / gl_FragCoord.w;
    #else
      float depth = gl_FragCoord.z / gl_FragCoord.w;
    #endif
      
    float fogFactor = smoothstep( fogNear, fogFar, depth );

    vec3 ofogc = min(fogColor*(50000.0/depth), fogColor);
    ofogc = max(ofogc, vec3(0.2, 0.2, 0.2));

    gl_FragColor.rgb = mix( gl_FragColor.rgb, ofogc, fogFactor );
  #endif
}

