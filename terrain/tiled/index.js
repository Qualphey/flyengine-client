
const Canvas = require("../canvas.js");
const Terrain = require("../terrain.js");
const TerrainLoader = require("../loader.js");

const WORLD = require("../../World.js").default;

module.exports = class extends Terrain  {
  static async construct(terrain_path, tile_callback) {
    try {
      console.log("WORLD", WORLD);
      const terrain = await TerrainLoader.load(terrain_path);
      let environment = {
        width: terrain.width,
        height: terrain.height,
        data: new Uint8Array(terrain.data.length)
      } 
//      var canvas = await Canvas.init(environment, terrain.width, terrain.height);
      //
      var img = new Image();
      img.src = "res/world/world.png";
      await new Promise(function(resolve) {
        img.onload = function() {
          resolve();
        };
      });

      let canvas = {
        element: document.createElement('canvas')
      }

      canvas.element.width = img.width
      canvas.element.height = img.height;

      console.log("CANVAS", img)

      var img_context = canvas.element.getContext( '2d' );

      img_context.scale(-1,1)      
      img_context.translate(img.width/2,img.height/2);
      img_context.rotate(180*Math.PI/180);

      img_context.drawImage(img, img.width/2,-img.width/2, img.width, img.height);


      let _this = new module.exports(
        terrain,
        canvas,
        {
          name: terrain_path,
          tile_size: 1800,
          height_multiplier: 10
        },
        tile_callback
      );

      
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(terrain_cfg, canvas, cfg, tile_callback) {
    super(terrain_cfg, canvas, cfg);

    this.tile_size = cfg.tile_size;
    this.width = terrain_cfg.width;
    this.height = terrain_cfg.height;

    this.tile_callback = tile_callback;

    var this_class = this;

//    this.grid = new TerritoryGrid();

    // TERRITORY CURSOR

    var tc_geometry = new THREE.PlaneGeometry(this.tile_size, this.tile_size);
    var tc_matrix4 = new THREE.Matrix4();
    tc_matrix4.compose(
      new THREE.Vector3(0, 0, 0),
      new THREE.Quaternion().setFromAxisAngle( new THREE.Vector3( 0, 0, 1 ), 1.5708 ),
      new THREE.Vector3(1, 1, 1)
    );

    tc_geometry.applyMatrix(tc_matrix4);

    var tc_material = new THREE.MeshBasicMaterial( { color: 0x3399CC } );
    tc_material.transparent = true;
    tc_material.opacity = 0.4;
    tc_material.needsUpdate = true;

    var territory_cursor = this.terr_cursor = this.tile_cursor = new THREE.Mesh(tc_geometry, tc_material);
    this.terr_cursor.name = "terr_cursor";
    territory_cursor.rotation.x = -1.5708;
    WORLD.scene.add( territory_cursor );

    window.addEventListener("mousemove", function(e) {
      if (WORLD.scene.mouseover && !WORLD.destroyed) {

        var mouse = new THREE.Vector2();
        mouse.x = ( e.clientX / window.innerWidth ) * 2 - 1;
        mouse.y = - ( e.clientY / window.innerHeight ) * 2 + 1;

        var raycaster = new THREE.Raycaster();
        raycaster.setFromCamera( mouse, WORLD.camera );

        var intersects = raycaster.intersectObject( this_class.mesh );

        for (var i = 0; i < intersects.length; i++) {
          var tile_coords = this_class.getCoords(intersects[i].point);
          var heights = this_class.getPlaneHeightsAt(tile_coords);

          var pos_y = this_class.getHeightAt(tile_coords.x, tile_coords.y);
          var cursor = new THREE.Vector3(intersects[i].point.x+this_class.tile_size/2, pos_y, intersects[i].point.z+this_class.tile_size/2);
          cursor.x = Math.round(cursor.x / this_class.tile_size)*this_class.tile_size;
          cursor.z = Math.round(cursor.z / this_class.tile_size)*this_class.tile_size;

          cursor.x -= this_class.tile_size/2;
          cursor.z -= this_class.tile_size/2;

          territory_cursor.position.set(cursor.x, 5, cursor.z);

          territory_cursor.geometry.vertices[ 0 ].z = heights[2];
          territory_cursor.geometry.vertices[ 1 ].z = heights[0];
          territory_cursor.geometry.vertices[ 2 ].z = heights[3];
          territory_cursor.geometry.vertices[ 3 ].z = heights[1];
          territory_cursor.geometry.verticesNeedUpdate = true;

        }
      }
    });

    let _this = this;

    
    this.click_listener = async function(e) {
      try {
        if (!_this.hidden && e.which == 1) {
          console.log("SELECT TILE");
          let intersects = WORLD.mouse.intersect(_this.mesh, e);
          if (intersects) {
            var cursor = new THREE.Vector3(intersects[0].point.x+_this.tile_size/2, intersects[0].point.y, intersects[0].point.z+_this.tile_size/2);
            cursor.x = Math.round(cursor.x / _this.tile_size)*_this.tile_size;
            cursor.z = Math.round(cursor.z / _this.tile_size)*_this.tile_size;

            let hw = Math.floor(_this.width/2)-1;
            let hh = Math.floor(_this.height/2)-1;

            _this.tile_callback(cursor.x / _this.tile_size + hw, cursor.z / _this.tile_size + hh);
          }
          

        }
      } catch (er) {
        console.error(er.stack);
      }
    };
    this.addEventListener("click", this.click_listener);

    const geometry = new THREE.CircleGeometry( 150000, 16 );
    var material = new THREE.MeshBasicMaterial({ color: 0x203282 });
    var plane = this.terr_plane = new THREE.Mesh( geometry, material );
    plane.name = "terr_plane_world"
    plane.position.y = -100;
    plane.rotation.x = -Math.PI / 2;
    WORLD.scene.add( plane );
  }

  hide() {
    this.removeEventListener("click", this.click_listener);
    WORLD.scene.remove(this.terr_plane);
    super.hide();
  }

  show() {
    this.addEventListener("click", this.click_listener);
    super.show();
  }

}
