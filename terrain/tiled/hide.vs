
uniform float hide_range;

varying vec2 v_texCoord;

void main() {

  vec2 pos2 = vec2(position.x, position.z);
  float distance = length(pos2);
  float ny = position.y;
  if (distance < hide_range) {
    ny = 0.0;
  } 
  
  // Pass the texcoord to the fragment shader.
  v_texCoord = uv;

  gl_Position = projectionMatrix *
                modelViewMatrix *
                  vec4(position.x, ny, position.z, 1.0);
}
