
const TerrainCanvas = require("../canvas.js");
const Terrain = require("../terrain.js");
const TerrainLoader = require("../loader.js");

const hide_vs = require('./hide.vs').default;
const hide_fs = require('./hide.fs').default;

const WORLD = require("../../World.js").default;

module.exports = class extends Terrain  {
  static async construct() {
    try {
      const region_size = 10, total_tiles = Math.pow(region_size, 2);


      let adj_terrain = {

      }

      let sx = 0;
      let sy = 0;

      let ex = 10;
      let ey = 10;
      

      let bi = 0;
      let vertex_buffer = undefined;
      for (let x = sx; x < ex; x++) {
        for (let y = sy; y < ey; y++) {
          const terrain = await TerrainLoader.load("res/world/region-"+x+"_"+y+"/region.bson");
          adj_terrain.width = terrain.width * region_size;
          adj_terrain.height = terrain.height * region_size;

          if (!vertex_buffer) vertex_buffer = new Uint8Array(terrain.data.length*total_tiles);
          for (let i = 0; i < terrain.height; i++) {
            let lx = x, ly = y;
            lx *= terrain.width;
            ly *= terrain.height;
            let li = ly * adj_terrain.width + lx;
            let buffer_index = li+i*adj_terrain.width;
            vertex_buffer.set(terrain.data.slice(i*terrain.width, i*terrain.width+terrain.width), buffer_index);
          }
          bi += terrain.data.length;
        }
      }

      console.log("HEIGHT BUFFER", vertex_buffer);
      adj_terrain.data = vertex_buffer;
/*
      var canvas = await TerrainCanvas.init({
        width: adj_terrain.width,
        height: adj_terrain.height,
        data: new Uint8Array(adj_terrain.data.length)
      }, adj_terrain.width, adj_terrain.height);*/

      var img = new Image();
      img.src = "res/world/world.png";
      await new Promise(function(resolve) {
        img.onload = function() {
          resolve();
        };
      });



      let canvas = {
        element: document.createElement('canvas')
      }

      canvas.element.width = img.width
      canvas.element.height = img.height;

      console.log("CANVAS", img)

      var img_context = canvas.element.getContext( '2d' );
      img_context.fillStyle = '#203282'
      img_context.fillRect(0, 0, img.width, img.height)

      let cmapx = 0;
      let cmapy = 0;


      img_context.scale(-1,1)
      
      img_context.translate(img.width/2,img.height/2);
      img_context.rotate(180*Math.PI/180);

      img_context.drawImage(img, cmapx, cmapy, img.width/10*region_size, img.height/10*region_size, img.width/2,-img.width/2, img.width, img.height);



      
      let _this = new module.exports(
        adj_terrain,
        canvas,
        {
          name: "far_terrain",
          tile_size: 450,
          height_multiplier: 7,
          vs: hide_vs,
          fs: hide_fs,
          disable_depth_write: true
        }
      );


      const geometry = new THREE.CircleGeometry( 400000, 16 );
      var material = new THREE.ShaderMaterial({
        color: 0x203282,
        uniforms: {
          fogColor: { type: "c" },
          fogNear: { type: "f" },
          fogFar: { type: "f" }
//          iColor: { value: THREE.Vector3(0.125, 0.196, 0.51) }
        },
        vertexShader: hide_vs,
        fragmentShader: hide_fs,
        fog: true
      });
      var plane = _this.terr_plane = new THREE.Mesh( geometry, material);
      plane.name = "terr_plane_far";
//      plane.position.y = -10000;
      plane.rotation.x = -Math.PI / 2;
      WORLD.scene.add( plane );

      
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(terrain_cfg, canvas, cfg) {
    super(terrain_cfg, canvas, cfg);

  }

  background(rx, ry, zx, zy) {
    this.mesh.scale.set(10, 10, 10);
    let mesh_pos_y =  -1500 - (this.data[(ry*10+zy) * this.width + rx*10+zx]*this.height_mult)*5;


    this.geometry.applyMatrix( new THREE.Matrix4().makeRotationY(180 * (Math.PI / 180)) );
    this.geometry.applyMatrix( new THREE.Matrix4().makeTranslation( -this.width*this.tile_size/2 + (rx*10+zx+0.75)*this.tile_size, 0, -this.width*this.tile_size/2 + (ry*10+zy+0.25)*this.tile_size) );
    this.mesh.rotation.set(0, 180 * (Math.PI / 180), 0);
    this.mesh.position.set(0, mesh_pos_y, 0);
    WORLD.zone_background = this;

    this.terr_plane.position.set(0, mesh_pos_y-500, 0);


  }

  destroy() {
    console.log("DESTROY FART");
    WORLD.scene.remove(this.terr_plane);
    super.destroy();
  }
}
