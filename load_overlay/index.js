
require("./theme.less");

module.exports = class {
  constructor() {
    this.element = document.createElement("div");
    this.element.classList.add("load_overlay");
    this.element.innerHTML = require("./body.html");
    this.shown = false;
  }

  show() {
    if (!this.shown) {
      document.body.appendChild(this.element);
      this.shown = true;
    }
  }

  hide() {
    if (this.shown) {
      document.body.removeChild(this.element);
      this.shown = false;
    }
  }
}
