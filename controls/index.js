
const WORLD = require("../World.js").default;

/**
 *  @class FlyEngine#Controls
 */
module.exports = class {
  constructor() {
    this.engine = WORLD;

    /**
     *  @name Controls#event_listener
     *  @type {Object} listeners
     */
    this.event_listeners = {
      click: [],
      mousemove: [],
      mousein: [],
      mouseout: [],
      mousedown: [],
      mouseup: []
    };
    this.cancel_cbs = [];
    
    let this_class = this;

    this.listener_prevent_fire = function(e) {
      e.preventDefault(e);
      this_class.fired(e, this);
    }

    this.listener_prevent = function(e) {
      e.preventDefault(e);
    }
    
    this.listener_fire = function(e) {
      this_class.fired(e, this);
    }


    window.addEventListener("click", this.listener_prevent_fire);
    window.addEventListener("auxclick", this.listener_prevent_fire);

    window.addEventListener("contextmenu", this.listener_prevent);

    window.addEventListener("mousedown", this.listener_fire);
    window.addEventListener("mouseup", this.listener_fire);
    window.addEventListener("mousemove", this.listener_fire);
  }

  on_cancel(cb) {
    this.cancel_cbs.push(cb);
  }

  cancel_action() {
    for (let c = 0; c < this.cancel_cbs.length; c++) {
      this.cancel_cbs[c]();
    }
  }
  
  fired(e, ctx) {
    let type = e.type;
    if (type == "auxclick") type = "click";
    if (type == "click") this.cancel_action();

    let skip = false;
    if (this.engine.controls.mouse_used_up && type == "mouseup") {
      this.engine.controls.mouse_used_up = false;
      skip = true;
    } else if (this.engine.controls.mouse_used_click && type == "click") {
      this.engine.controls.mouse_used_click = false;
      skip = true;
    }

    if (!skip && this.engine.scene.mouseover) {
      var mouse = new THREE.Vector2();
      mouse.x = ( e.clientX / window.innerWidth ) * 2 - 1;
      mouse.y = - ( e.clientY / window.innerHeight ) * 2 + 1;
      
      let mesh_list = [];

      for (let l = 0; l < this.event_listeners[type].length; l++) {
        let listener = this.event_listeners[type][l];
        mesh_list.push(listener.obj.mesh);
      }
      var raycaster = new THREE.Raycaster();
      raycaster.setFromCamera( mouse, this.engine.camera );

      var intersects = raycaster.intersectObjects(mesh_list);
      if (type == "mousemove") {
        for (let l = 0; l < this.event_listeners["mousein"].length; l++) {
          let listener = this.event_listeners["mousein"][l];
          if (intersects[0] && intersects[0].object == listener.obj.mesh && !listener.obj.mouseover) {
            type = "mousein";
            listener.obj.mouseover = true;
            break;
          }
        }
        for (let l = 0; l < this.event_listeners["mouseout"].length; l++) {
          let listener = this.event_listeners["mouseout"][l];
          if ((!intersects[0] || intersects[0].object != listener.obj.mesh) && listener.obj.mouseover) {
            type = "mouseout";
            listener.obj.mouseover = false;
            break;
          }

        } 
      }

   //   console.log("over", type);

      for (let l = 0; l < this.event_listeners[type].length; l++) {
        let listener = this.event_listeners[type][l];
        e.obj = listener.obj;
        if (intersects[0]) e.point = intersects[0].point;

        if (intersects[0] && intersects[0].object == e.obj.mesh) {
          if (
            type == "mousemove" ||
            type == "mousein" ||
            type == "click" ||
            type == "mousedown" ||
            type == "mousup"
          ) {
            listener.next(e);
          }
          break;
        } else if (type == "mouseout") {
          listener.next(e);
          break;
        }
      }
    }
  }

  destroy() {
    window.removeEventListener("click", this.listener_prevent_fire);
    window.removeEventListener("auxclick", this.listener_prevent_fire);

    window.removeEventListener("contextmenu", this.listener_prevent);

    window.removeEventListener("mousedown", this.listener_fire);
    window.removeEventListener("mouseup", this.listener_fire);
    window.removeEventListener("mousemove", this.listener_fire);
  }
}
