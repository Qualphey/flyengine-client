

export default new (class {
  constructor() {
    this.paused = false;
    this.next_listener_index = {
      render: 0,
      animate: 0
    };
    this.event_listeners = {
      render: {},
      animate: {}
    };
  }

  async construct(config) {
    try {


      this.renderer = new THREE.WebGLRenderer();
      this.renderer.setClearColor(0xddddff);
      this.renderer.setPixelRatio(window.devicePixelRatio);

      var size_dec = 4;
      var win_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)-size_dec;
      var win_height= Math.max(document.documentElement.clientHeight, window.innerHeight || 0)-size_dec;
      this.renderer.setSize(win_width, win_height);

      if (config.graphics.shadows) {
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
      }

      this.camera = new THREE.PerspectiveCamera(
        60, win_width / win_height, 1, 1000000
      );

      const _this = this;

      window.addEventListener('resize', function() {
        win_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)-size_dec;
        win_height= Math.max(document.documentElement.clientHeight, window.innerHeight || 0)-size_dec;
        _this.camera.aspect = win_width / win_height;
        _this.camera.updateProjectionMatrix();
        _this.renderer.setSize(win_width, win_height);
      }, false);


      this.scene = new THREE.Scene();
      this.scene.background = new THREE.Color( 0xddddff );
 //     this.scene.fog = new THREE.Fog( 0xCCCCCC, 8000, 30000 );

      this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
      this.controls.userPanSpeed = 100;

      this.container = document.getElementById('container');
      this.container.innerHTML = "";
      this.container.appendChild(this.renderer.domElement);

      this.container.addEventListener('mouseover', function(e) {
        _this.scene.mouseover = true;
      });

      this.container.addEventListener('mouseout', function(e) {
        _this.scene.mouseover = false;
      });

      this.stats = new Stats();
      this.container.appendChild( this.stats.dom );
  /*
      if ( ! Detector.webgl ) {
        Detector.addGetWebGLMessage();
        container.innerHTML = "WebGL Not Supported!";
      }
  */
      var light = new THREE.DirectionalLight( 0xffffff, 0.5 );
      light.position.set( 1500, 2000, 1500 );

      var val = 5000;

      light.shadowCameraFar = 8000;
      light.shadowCameraLeft = -val;
      light.shadowCameraRight = val;
      light.shadowCameraTop = val;
      light.shadowCameraBottom = -val;

      light.target.position.set( 0, 0, 0 );
      light.castShadow = true;
      light.shadow.mapSize.width = 1024;
      light.shadow.mapSize.height = 1024;
      this.scene.add( light );

    //  scene.fog = new THREE.Fog( 0xddddff, 1000, 5000 );

      // LIGHTS TODO
      let hemiLight = new THREE.HemisphereLight( 0xddddff, 0xddddff, 0.7 );
      hemiLight.color.setHSL( 1, 1, 1 );
      hemiLight.groundColor.setHSL( 1, 1, 1 );
      hemiLight.position.set( 100, 500, 100 );
      this.scene.add( hemiLight );

      this.textures = {
        water: await new Promise((out) => {
          var loader = new THREE.TextureLoader();
          loader.load('res/water.png', function (texture) {

            out(texture);
  /*
              texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
              texture.offset.set( 0, 0 );
              texture.repeat.set( 1024, 1024 );
              var geometry = new THREE.PlaneGeometry( 150000, 150000, 1 );
              var material = new THREE.MeshBasicMaterial({ map: texture });
              var plane = new THREE.Mesh( geometry, material );
              _this.terr_plane = plane;
              plane.name = "terr_plane"
              plane.position.y = -100;
              plane.rotation.x = -Math.PI / 2;
              _this.scene.add( plane );
            */
            },
            // Function called when download progresses
            function ( xhr ) {
          //		console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );
            },
            // Function called when download errors
            function ( xhr ) {
            //	console.log( 'An error happened' );
            }
          );
        })
      };



        //Create a helper for the shadow camera (optional)
/*      var helper = this.helper = new THREE.CameraHelper( light.shadow.camera );
      this.scene.add( helper );
*/




      const terrain_cfg = {
        tile_size: 300,
        height_multiplier: 3
      };
      const territory_cfg = {
        tile_size: 300,
        height_multiplier: 30
      };

      function animate() {
        requestAnimationFrame(animate);
        render();
        _this.stats.update();
      }

      function render() {
        if (!_this.paused && !_this.loading) {
          if (!_this.destroyed) {
            var delta = _this.clock.getDelta();
            /*
            if (_this.player) {
              _this.player.update(delta);
              if (_this.terrain.terropen) {
                _this.terrain.cur_area[4].updateFOW();
                _this.environment.cur_territory.updateFOW();
              } else {
                _this.terrain.updateFOW();
                _this.environment.updateFOW();
              }
            }*/
          //  WORLD.units.update(delta);
            _this.controls.update(delta);
     //       world.structures.render();
          }
          _this.renderer.render( _this.scene, _this.camera );
     //     world.astar.easystar.calculate();

     //     if (player.construct) player.construct.update();

          for (const li in _this.event_listeners["render"]) {
            _this.event_listeners["render"][li]({ delta: delta });
          }
        }
      }
      animate();
        
    } catch (e) {
      console.error(e.stack);
    }
  }



  addEventListener(event, listener) {
    let li = this.next_listener_index[event];
    this.event_listeners[event][li] = listener;
    this.next_listener_index[event]++;
    return li;
  }

  removeEventListener(event, li) {
    delete this.event_listeners[event][li];
  }

  destroy() {
    this.destroyed = true;
    this.game_controls.destroy();
    this.astar.destroy();
    this.player.destroy();
    this.structures.destroy();
    this.scene.remove(this.helper);
    this.scene.remove(this.terr_plane);
    this.terrain.destroy();
    this.environment.destroy();
    this.renderer.dispose();
    console.log("DESTROY WORLD");
  }
})();
