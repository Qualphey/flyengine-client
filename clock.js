'use strict'


/**
 * @class Clock
 * @arg {int} time
 */
module.exports = class {
  constructor(time) {
    this.element = document.createElement('div');
    this.element.classList.add('main_bar');
    document.body.appendChild(this.element);

    this.time = time;
    this.last_time = Date.now();

    var date = new Date(time);
    var year = date.getFullYear();
    var month = ('0' + (date.getMonth() + 1)).slice(-2);
    var day = ('0' + date.getDate()).slice(-2);
    var hours = ('0' + date.getHours()).slice(-2);
    var minutes = ('0' + date.getMinutes()).slice(-2);
    var seconds = ('0' + date.getSeconds()).slice(-2);
    this.element.innerHTML = "Server Time: "+year+'-'+month+'-'+day+' '
    +hours+':'+minutes+':'+seconds;
  }

  static async init(socket) {
    console.log('world.ui-request', 'time');
    socket.emit('world.ui-request', 'time');
    var time = await new Promise(function(resolve) {
      var resolved = false;
      socket.on('world.io-response', function(data) {
        if (data.what === 'time') {
          resolve(data.time);
        } else {
          console.error("Invalid response type:", data.what);
          resolve(undefined);
        }
        resolved = true;
        clearTimeout(timeout_id);
      });
      const timeout_id = setTimeout(timeout, 15000);
      function timeout() {
        if (!resolved) {
          console.error("Server response timeout...");
          resolve(undefined);
        }
      }
    });

    return new module.exports(time);
  }

  getDelta() {
    var new_time = Date.now();
    var delta = new_time-this.last_time;
    this.last_time = new_time;
    this.time += delta;

    var date = new Date(this.time);
    var year = date.getFullYear();
    var month = ('0' + (date.getMonth() + 1)).slice(-2);
    var day = ('0' + date.getDate()).slice(-2);
    var hours = ('0' + date.getHours()).slice(-2);
    var minutes = ('0' + date.getMinutes()).slice(-2);
    var seconds = ('0' + date.getSeconds()).slice(-2);
    this.element.innerHTML = "Server Time: "+year+'-'+month+'-'+day+' '
    +hours+':'+minutes+':'+seconds;

    return delta/1000;
  }
}
