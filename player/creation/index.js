
require("./theme.less");

module.exports = class {
  constructor(next) {
    this.element = document.createElement("div");
    this.element.classList.add("player_creation");
    this.element.innerHTML = require("./body.html");
    document.body.appendChild(this.element);

    var this_class = this;
    var name_input = this.element.querySelector('input');
    name_input.addEventListener("keyup", function(e) {
      if (e.key === "Enter") {
        next(name_input.value);
        document.body.removeChild(this_class.element);
      }
    });
  }
}
