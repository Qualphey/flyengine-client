'use strict';

module.exports = class {
  constructor(config, func) {
    let this_call = this;
    var button = document.createElement("div");
    button.style.backgroundImage = "url('"+config.image_src+"')";
    button.classList.add("ability_button");
    button.addEventListener('click', function() {
      func(this_call);
    });

    var tooltip = document.createElement("div");
    tooltip.innerHTML = config.tooltip;
    tooltip.classList.add("tooltip");
    tooltip.style.top = "100px";
    tooltip.style.backgroundColor = "#777777";
    button.addEventListener('mouseover', function(e) {
      document.body.appendChild(tooltip);
    });
    button.addEventListener('mouseout', function(e) {
      document.body.removeChild(tooltip);
    });

    this.button = button;
  }

  
}
