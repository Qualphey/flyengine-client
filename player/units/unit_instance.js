'use strict';

var WORLD = global.WorldUI;

var Unit = require("./unit.js")
var Controler = require("../territory/controler.js")

module.exports = class {
  constructor(docs) {
    var terrain = WORLD.terrain;
    var camera = WORLD.camera;
    var models = global.models;

    this.list = [];

    var this_class = this;

    for (var u = 0; u < docs.length; u++) {
      var un = docs[u];
      un.color = 0xFFFFFF;
      un.owned = true;

      var unit = new Unit(un);
      this_class.list.push(unit);
    }

    this.controler = new Controler(this);
  }

  findById(id) {
    var found = false;
    for (var u = 0; u < this.list.length; u++) {
      if (id === this.list[u].id) {
        found = this.list[u];
      }
    }
    return found;
  }

  place(what, where) {
    var unit = new Unit({
      name: what,
      model : this.models[what],
      x : where.x, y : where.y,
      color: 0xFFFFFF
    }, this.terrain, this.camera);
    this.list.push(unit);
  }

  update(delta) {
    for (let u = 0; u < this.list.length; u++) {
      this.list[u].update(delta, this.structures);
    }
  }
}
