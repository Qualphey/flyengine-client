
module.exports = class {
  constructor(name, amount, inv) {
    this.name = name;
    this.amount = amount;

    this.block = document.createElement("div");
    this.block.innerHTML = name+" : "+amount;

    this.drop_btn = document.createElement("button");
    this.drop_btn.innerHTML = "drop";

    let socket = inv.unit.player.socket;
    this.drop_btn.addEventListener("click", function() {
      socket.emit("world.ui-command", {
        command: "unit_drop",
        what: name,
        id: inv.unit.id
      });
    });

    this.block.appendChild(this.drop_btn);

  }

  update_block() {
    this.block.innerHTML = this.name+" : "+this.amount;
    this.block.appendChild(this.drop_btn);
  }
}
