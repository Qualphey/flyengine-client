const Item = require("./item");

module.exports = class {
  constructor(items, unit) {
    this.unit = unit;
    this.displayed = false;

    this.block = document.createElement("div");
    this.block.classList.add("inventory_block");

    this.items = {};
    for (let i in items) {
      let amount = items[i];
      this.items[i] = new Item(i, amount, this);
    }

    this.update();
  }

  update() {
    this.block.innerHTML = "";
    for (let i in this.items) {
      this.block.appendChild(this.items[i].block)
    }
  }

  reconstruct(items) {
    this.items = {};
    for (let i in items) {
      let amount = items[i];
      this.items[i] = new Item(i, amount, this);
    }

    this.update();
  }

  add(what, amount) {
    if (!this.items[what]) {
      this.items[what] = new Item(what, amount, this);
    } else {
      this.items[what].amount++;
      this.items[what].update_block();
    }
  }

  display() {
    if (!document.body.contains(this.block)) {
      document.body.appendChild(this.block);
      this.displayed = true;
    }
  }

  hide() {
    if (document.body.contains(this.block)) {
      document.body.removeChild(this.block);
      this.displayed = false;
    }
  }

  all() {
    let obj = {};
    for (let name in this.items) {
      obj[name] = this.items[name].amount
    }
    return obj;
  }
}
