
const Creation = require("./creation/index.js");

const Unit = require("../characters/controlled/index.js");

const units_gui_html = require("./units/units_gui.html"); 


const WORLD = require("../World.js").default;
/**
  @class Player
*/
module.exports = class {

  static async init(socket) {
    console.log("INIT PLAYER");
    var cfg = await new Promise(function(resolve) {
      socket.emit('world.ui-request_player_data', false);
      socket.on('world.io-player_data', function(player) {
        resolve(player);
      });
    });
    console.log("RESP");

    console.log(cfg);

    if (cfg.init) {
      const new_player_name = await new Promise(function(resolve) {
        new Creation(function(name) {
          resolve(name);
        });
      });

      socket.emit('world.ui-request_init_player', new_player_name);
    }

    let units = cfg.player.units;

    let this_class = new module.exports(socket);
    this_class.units = [];

    for (let u = 0; u < units.length; u++) {
      this_class.units.push(await Unit.init(units[u], this_class));
    }
/*
    let constructions = cfg.player.constructions;
    WORLD.structures.set_config(cfg.player.struct_config);
    for (let c = 0; c < constructions.length; c++) {
      await WORLD.structures.add_construction(constructions[c]);
    }

    let structs = cfg.player.structures;
    for (let c = 0; c < structs.length; c++) {
      await WORLD.structures.add_structure(structs[c]);
    }
*/
    return this_class;
  }

  constructor(socket, world) {
    this.socket = socket;
    this.world = world;
    this.units = [];
    this.selected_units = [];

    let units_gui = this.units_gui = document.createElement("div");
    units_gui.classList.add("units_gui");
    units_gui.innerHTML = units_gui_html;
    units_gui.selected_units = units_gui.querySelector(".selected_units");
    units_gui.action_bar = units_gui.querySelector(".action_bar");
    
    let build_btn = units_gui.action_bar.querySelector("button[name='build']");
    let build_menu = units_gui.querySelector(".build_menu");
    
    let this_class = this;
/*
    console.log("WORLD STRUCTURES", world.structures);
    for (let i = 0; i < world.structures.menu.length; i++) {
      let build_stockpile = document.createElement("img");
      build_stockpile.src = world.structures.menu[i].menu_img;
      build_menu.appendChild(build_stockpile);
      build_stockpile.addEventListener("click", async function(e) {
        try {
          if (world.terrain.terropen) {
            this_class.construct = await world.structures.build(world.structures.menu[i]);
          }
        } catch (e) {
          console.error(e.stack);
        }
      });
    }
*/
    build_btn.addEventListener("click", function(e) {
      if (build_menu.style.display != "block") {
        build_menu.style.display = 'block';
      } else {
        build_menu.style.display = '';
      }
    });

    let inv_btn = units_gui.action_bar.querySelector("button[name='inventory']");

    inv_btn.addEventListener("click", function(e) {
      let inv = this_class.selected_units[0].inventory;
      if (inv.displayed) {
        inv.hide();
      } else {
        inv.display();
      }
    });

    socket.on("world.io-command_response", async function(data) {
      switch (data.command) {
        case "unit_move":
          for (let u = 0; u < this_class.selected_units.length; u++) {
            if (data.id == this_class.selected_units[u].id) {
              console.log(data.path);
              console.log("MOVE TO MAKE ACTION:", data.action, data.what);
              this_class.selected_units[u].move(
                data.path,
                { action: data.action, what: data.what }
              );
            }
          }
          break;
        case "build":
          await world.structures.add_construction(data);
          await world.structures.update();
          break;
        case "unit_gather":
          let unit = this_class.select_unit(data.id);
          if (data.err) {
            unit.animate("Idle", THREE.LoopPingPong)
          } else {
            unit.inventory.add(data.what, 1);
            unit.inventory.update();
          }
          break;
        case "unit_store_res":
          let unit_store_res = this_class.select_unit(data.unit_id);
          unit_store_res.inventory.reconstruct(data.unit_inv);
          let unit_store_struct = this_class.world.structures.select_structure(data.structure_id);
          unit_store_struct.update_res(data.structure_res);
          break;
        case "unit_bringres":
          let unita = this_class.select_unit(data.unit_id);
          unita.inventory.reconstruct(data.unit_inv);
          break;
        case "unit_construct":
          console.log("contruct_res", data);
          if (data.err) {
            let unit = this_class.select_unit(data.unit_id);
            unit.animate("Idle", THREE.LoopPingPong)
          } else {
            if (!data.finished) {
              let construction = this_class.world.structures.select_construction(data.construct_id);
              construction.progress(data.res_available, data.res_used);
            } else {
              this_class.world.structures.remove_construction(data.construct_id);
              await this_class.world.structures.add_structure(data.finished);

              let unit = this_class.select_unit(data.unit_id);
              unit.animate("Idle", THREE.LoopPingPong)
            }
          }
          break;
        case "unit_drop":
          let unitb = this_class.select_unit(data.id);
          unitb.inventory.reconstruct(data.ninv);
          break;
        case "destroy":
          console.log("DETROY RESP");
          this_class.world.structures.remove_construction(data.construction_id);
          break;
        default:
      }
    });
  }

  select_unit(id) {
    for (let u = 0; u < this.units.length; u++) {
      if (this.units[u].id == id) {
        return this.units[u];
      }
    }
  }

  vision_points() {
    let vision_points = [];

    if (this.world.terrain.terropen) {
      for (let u = 0; u < this.units.length; u++) {
        let unit = this.units[u];
        if (this.world.terrain.curterr_contains(unit.x, unit.z)) {
          vision_points.push(new THREE.Vector2(
            unit.object.position.x, unit.object.position.z
          ));
        }
      }
      for (let s = 0; s < this.world.structures.constructions.length; s++) {
        let structure = this.world.structures.constructions[s];
        if (this.world.terrain.curterr_contains(structure.x, structure.y)) {
          vision_points.push(new THREE.Vector2(
            structure.object.position.x, structure.object.position.z
          ));
        }
      }
      for (let s = 0; s < this.world.structures.list.length; s++) {
        let structure = this.world.structures.list[s];
        if (this.world.terrain.curterr_contains(structure.x, structure.y)) {
          vision_points.push(new THREE.Vector2(
            structure.object.position.x, structure.object.position.z
          ));
        }
      }
    } else {
      for (let u = 0; u < this.units.length; u++) {
        let unit = this.units[u];
        vision_points.push(new THREE.Vector2(
          unit.object.position.x, unit.object.position.z
        ));
      }
    }
    if (vision_points.length == 0 ) {
      vision_points = [
        new THREE.Vector2(999999, 999999),
      ]
    }
    return vision_points;
  }

  async move_units(point) {
    try {
      for (let u = 0; u < this.selected_units.length; u++) {
        let path = await this.selected_units[u].find_path(point);

        console.log("path", path);
        this.socket.emit("world.ui-command", {
          command: "unit_move",
          id: this.selected_units[u].id,
          path: path,
          x: point.x,
          y: point.z
        })
      }
    } catch (e) {
      console.error(e.stack);
    }
  }


  update(delta) {
    if (this.selected_units.length > 0) {
      if (!document.body.contains(this.units_gui)) {
        this.units_gui.selected_units.innerHTML = "";
        document.body.appendChild(this.units_gui);
      }

      for (let u = 0; u < this.selected_units.length; u++) {
        let unit = this.selected_units[u]
        if (!this.units_gui.selected_units.contains(unit.gui_block)) {
          this.units_gui.selected_units.appendChild(unit.gui_block)
        }
      }

    } else {
      if (document.body.contains(this.units_gui)) {
        document.body.removeChild(this.units_gui);
      }
    }
/*
    for (let u = 0; u < this.units.length; u++) {
      this.units[u].update(delta);
    }*/
  }


  destroy() {
    for (let u = 0; u < this.units.length; u++) {
      this.units[u].destroy();
    }
    this.units = undefined;
  }
}
