

module.exports = class {
  static async construct() {
    try {
      let _this = new module.exports();

      
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor() {

  }

  async command(name, data) {
    try {
      this.socket.emit("player-command-"+name, data);
    } catch (e) {
      console.error(e.stack);
    }
  }
}
