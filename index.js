
const Structures = module.exports.Structures = require("./structures/index.js");
const Terrain = module.exports.Terrain = require("./terrain/index.js");
const Environment = module.exports.Environment = require("./environment/index.js");


const XHR = require('core/utils/xhr_async.js');
const Models = require('./models/index.js');

//const Terrain = require('./terrain/index.js');
//const Environment = require('./environment/index.js');

const Clock = require('./clock.js');

const LoadOverlay = require("./load_overlay/index.js");

const Player = require("./player/index.js");


//const Structures = require("./structures/index.js");

const AStar = require("./astar/index.js");
const Mouse = require("./Mouse.js");
const Controls = require("./controls/index.js");



const io = require("socket.io-client");

const WORLD = require("./World.js").default;
/**
 * @class World
 * @arg {Object} config
 * @arg {Clock} clock
 * @arg {Object} load_overlay
 */
module.exports = class {
  constructor(config, clock, load_overlay) {

  }

  static async init(cfg) {
  console.log("HERE");
    try {
      if (!cfg) cfg = {};
      var config = JSON.parse(await CORDOVA_OPEN_FILE("www/pages/game/config.json", "app"));
      console.log("CONFIG", config);
      if (config) {
        var load_overlay = new LoadOverlay();
        load_overlay.show();

        var socket = await new Promise(function(resolve) {
//          var client_socket = io("http://127.0.0.1:9369");
          console.log("IO CONNECT", config.game_host);
          var client_socket = io(config.game_host, {
            auth: {
              token: FLYAUTH_USERID
            }
          });
          var resolved = false;

          client_socket.on('connect', function() {
            console.log('SOCKET CONNECTED');
            resolved = true;
            resolve(client_socket);
          });

          client_socket.on('disconnect', function(){
            console.log('SOCKET DISCONNECTED');
          });

          client_socket.on("unauthorized", function(data) {
            console.log("UNAUTHORIZED");
          //  window.location.replace("/signin");
            if (cfg.unauthorized) cfg.unauthorized();
            window.location.replace("/signin/index.html");
          });


          client_socket.on('access_denied', function(data) {
            window.location.replace("http://127.0.0.1:9369/signin.html");
          });

          const timeout_id = setTimeout(timeout, 15000);
          function timeout() {
            if (!resolved) {
              console.error("Server connection timeout...");
              resolve(undefined);
            }
          }
        });

        WORLD.player = await Player.init(socket);
        WORLD.game_controls = new Controls();
        WORLD.mouse = new Mouse();
        WORLD.clock = await Clock.init(socket);
        await WORLD.construct(config);

        var models = new Models();
        await models.load(config.models);
        WORLD.models = models;
        console.log("MODELS", models);

/*
        var terrain = await Terrain.init('res/world', world, terrain_cfg, territory_cfg);
        world.terrain = terrain;
        console.log("TERRAIN", terrain);
        world.astar = new AStar(810, 810, world); // env node should be 18x18

        var environment = await Environment.init(world, terrain_cfg, territory_cfg);
        world.environment = environment;

        world.structures = await Structures.init(world);
        let 
        world.player = player;

*/

        load_overlay.hide();

        let sbox_timeout = false; 
        let cur_opt_div;

        WORLD.game_controls.on_cancel(function() {
          if (WORLD.opt_div && document.body.contains(WORLD.opt_div)) {
            document.body.removeChild(world.opt_div);
            WORLD.opt_div = undefined;
          }
          if (WORLD.controls.mouse_used) {
            WORLD.controls.mouse_used = false;
          } else {
            if (!WORLD.selection_box && WORLD.scene.mouseover) {
              if (sbox_timeout) clearTimeout(sbox_timeout)
            }
          }
        });
        
        let selection_box = document.createElement("div");
        selection_box.classList.add("selection_box");
/*
        let down_x, down_y;
        window.addEventListener("mousedown", function(e) {
          if (e.which == 1 && WORLD.scene.mouseover) {
            down_x = e.clientX
            down_y = e.clientY

            sbox_timeout = setTimeout(function() {
              WORLD.selection_box = true;  
              document.body.appendChild(selection_box);
              mouse_move(e);
            }, 300);
          } else if (e.which == 3) {
            if (player.construct) {
              player.construct.cancel();
              player.construct = undefined;
            }
          }
        });

        window.addEventListener("mousemove", mouse_move);
        async function mouse_move(e) {
          try {
        //    await WORLD.mouse.set_position(e);

            if (WORLD.selection_box && document.body.contains(selection_box)) {


              var mouse = new THREE.Vector2();
              mouse.x = ( e.clientX / window.innerWidth ) * 2 - 1;
              mouse.y = - ( e.clientY / window.innerHeight ) * 2 + 1;
              mouse.x = e.clientX
              mouse.y = e.clientY

              if (mouse.x < down_x) {
                selection_box.style.left = mouse.x+"px";
              } else {
                selection_box.style.left = down_x+"px";
              }

              if (mouse.y < down_y) {
                selection_box.style.top = mouse.y+"px";
              } else {
                selection_box.style.top = down_y+"px";
              }
              
              selection_box.style.width = Math.abs(mouse.x-down_x)+"px";
              selection_box.style.height = Math.abs(mouse.y-down_y)+"px";
            } else if (WORLD.scene.mouseover && player.construct && WORLD.terrain.terropen) {
              player.construct.update();
            }
          } catch(e) {
            console.error(e.stack);
          }         
        }

        window.addEventListener("mouseup", function(e) {

          if (document.body.contains(selection_box) && e.which == 1) {

            const bbox = {
              minX: Math.min(down_x, e.clientX),
              minY: Math.min(down_y, e.clientY),
              maxX: Math.max(down_x, e.clientX),
              maxY: Math.max(down_y, e.clientY),
              contains: function(cx, cy) {
                if (this.minX < cx && this.minY < cy && cx < this.maxX && cy < this.maxY) {
                  return true;
                } else {
                  return false
                }
              }
            }
          //  console.log(world.player.units);
            for (let u = 0; u < player.units.length; u++) {
              if (player.units[u]) {
                let coords = player.units[u].screen_coords()
                if (bbox.contains(coords.x, coords.y)) {
                  player.units[u].select();
                  console.log("YEP");
                }
              }
            }
            console.log("mouseup");

            document.body.removeChild(selection_box);
            world.selection_box = false;
          }
        });
*/
        return WORLD;
      } else {
        throw "Failed to load config";
      }
    } catch(e) {
      console.error(e.stack);
      return false;
    }
  }




}
