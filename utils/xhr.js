'use strict'

module.exports = class {
  static async get(url, params) {
    if (params) {
      url += "?data="+encodeURIComponent(JSON.stringify(params));
    }
    var xhr = new XMLHttpRequest();

    return await new Promise(function(resolve) {
      xhr.addEventListener("load", function(e) {
        resolve(this.responseText);
      });
      xhr.open("GET", url);
      xhr.send();
    });
  }

  static async post(url, params) {
    if (params.formData) {
      console.log("FORM DATA");
      var xhr = new XMLHttpRequest();
      return await new Promise(function(resolve) {
        xhr.addEventListener("load", function(response) {
          resolve(response);
        });
        xhr.open("POST", url);
        xhr.send(params.formData);
      });
    //  xhr.setRequestHeader("Content-Type","multipart/form-data");
      console.log(url);
    } else {
      var xhr = new XMLHttpRequest();
      return await new Promise(function(resolve) {
        //Send the proper header information along with the request
        xhr.onreadystatechange = function() {//Call a function when the state changes.
          if(xhr.readyState == 4 && xhr.status == 200) {
            resolve(xhr.responseText);
          }
        }
        var json = JSON.stringify(params);
        var param_str = 'data='+encodeURIComponent(json);

        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(param_str);
      });
    }
  }

  static getParamByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }
}
