'use strict'


module.exports = class {
  constructor(next) {
    var this_class = global.WorldUI.clock = this;
    var WORLD = global.WorldUI;
    console.log('worldmod.ui-request', 'time');
    WORLD.socket.emit('worldmod.ui-request', 'time');
    WORLD.socket.on('worldmod.io-response', function(data) {
      this_class.main_bar = document.createElement('div');
      this_class.main_bar.classList.add('main_bar');
      document.body.appendChild(this_class.main_bar);

      if (data.what === 'time') {
        this_class.time = data.time;
        this_class.last_time = Date.now();

        var date = new Date(this_class.time);
        var year = date.getFullYear();
        var month = ('0' + date.getMonth()).slice(-2);
        var day = ('0' + date.getDate()).slice(-2);
        var hours = ('0' + date.getHours()).slice(-2);
        var minutes = ('0' + date.getMinutes()).slice(-2);
        var seconds = ('0' + date.getSeconds()).slice(-2);
        this_class.main_bar.innerHTML = "Server Time: "+year+'-'+month+'-'+day+' '
        +hours+':'+minutes+':'+seconds;

        next();
      }
    });
  }

  getDelta() {
    var new_time = Date.now();
    var delta = new_time-this.last_time;
    this.last_time = new_time;
    this.time += delta;

    var date = new Date(this.time);
    var year = date.getFullYear();
    var month = ('0' + date.getMonth()).slice(-2);
    var day = ('0' + date.getDate()).slice(-2);
    var hours = ('0' + date.getHours()).slice(-2);
    var minutes = ('0' + date.getMinutes()).slice(-2);
    var seconds = ('0' + date.getSeconds()).slice(-2);
    this.main_bar.innerHTML = "Server Time: "+year+'-'+month+'-'+day+' '
    +hours+':'+minutes+':'+seconds;

    return delta/1000;
  }
}
