

const intersects = require("intersects");

const Tile = require("./Tile.js");

module.exports = class {
  static async init() {

  }

  constructor(width, height, world) {
    this.world = world;
    let tw = world.terrain.width, th = world.terrain.height;
    this.tiles = [];
    this.width = world.terrain.width;
    this.height = world.terrain.height;


    for (let tx = 0; tx < world.terrain.width; tx++) {
      for (let ty = 0; ty < world.terrain.height; ty++) {
        this.tiles.push(new Tile(tx-tw/2, ty-th/2, width, height, world));
      }
    }
  }

  async find_path(start, dest) {
    try {
      let path = [];
      let path_tiles = [];
      for (let t = 0; t < this.tiles.length; t++) {
        let tile = this.tiles[t];
        let lbi = intersects.lineBox(
          start.x, start.y,
          dest.x, dest.y,
          tile.min_x, tile.min_y,
          this.world.terrain.tile_size*50, this.world.terrain.tile_size*50
        );
        if (lbi) {
          path_tiles.push(tile);
      //    console.log("intersects tile", tile, lbi);
        }
      }

      // Sort tiles. Closest to start at 0
      path_tiles.sort(function(ta, tb) {
        let taa = start.x - ta.x;
        let tab = start.y - ta.y;
        let tal = Math.sqrt( taa*taa + tab*tab );

        let tba = start.x - tb.x;
        let tbb = start.y - tb.y;
        let tbl = Math.sqrt( tba*tba + tbb*tbb );

        if (tal > tbl) {
          return 1;
        } else {
          return -1;
        }
      });

      let line = {
        a: {
          x: start.x,
          y: start.y
        },
        b: {
          x: dest.x,
          y: dest.y
        }
      };
   //   console.log("PATH TILES", path_tiles);

      let cur_start = start;

      if (path_tiles.length > 1) {
        for (let t = 1; t < path_tiles.length; t++) {
          let cur_tile = path_tiles[t-1];
          let next_tile = path_tiles[t];

          cur_tile.updateGrid();
          next_tile.updateGrid();

          let points = this.find_tile_connection(cur_tile, next_tile, line);
         // console.log("THE CONNECTION POINT", points);

          let npath = await cur_tile.find_path(cur_start, points.a);
        //  console.log("NPATH", npath, cur_start, points.a);

          path = [...path, ...npath];
          cur_start = points.b;

    //      this.add_debug_mesh(points.a.x, points.a.y);
    //      this.add_debug_mesh(points.b.x, points.b.y);
        }
      } else if (path_tiles.length > 0) {
        let cur_tile = path_tiles[0];
        cur_tile.updateGrid();
        let npath = await cur_tile.find_path(start, dest);
      //  console.log("NPATH (single tile)", npath, start, dest);

        path = [...path, ...npath];

      }
     // console.log("PATH >>>>>>>>>>>>>> ", path);
      return path;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  find_tile_connection(tile_a, tile_b, line) {
     //   console.log(tile_a);
    let terra = 50/tile_a.width*this.world.terrain.tile_size;
    let tx = undefined, ty = undefined;
    let points = {
   
    };
    if (tile_a.x < tile_b.x) { // X++
      tx = tile_a.width-1;

    } else if (tile_a.x > tile_b.x) { // X--
      tx = 0;
    }

    if (tile_a.y < tile_b.y) { // Y++
      ty = tile_a.height-1;

    } else if (tile_a.y > tile_b.y) { // Y--
      ty = 0;
    }


    if (tx != undefined) { 
      for (ty = 0; ty < tile_a.height; ty++) {
        let wtx = tile_a.x+(tx-tile_a.width/2)*terra;
        let wty = tile_a.y+(ty-tile_a.height/2)*terra;
      //  console.log(wtx, wty);
        if (intersects.lineBox(
          line.a.x, line.a.y, line.b.x, line.b.y,
          wtx, wty, terra, terra 
        )) {
          if (tile_a.grid[ty][tx] == 0 && tile_b.grid[ty][0] == 0) {
            if (tile_a.x < tile_b.x) { // X++
            //  console.log("X++", tile_a.x, tile_b.x);
              points.a = {
                x: wtx+terra/2,
                y: wty+terra/2
              }
              points.b = {
                x: points.a.x+terra,
                y: wty+terra/2
              }
            } else if (tile_a.x > tile_b.x) { // X--
            //  console.log("X--", tile_a.x, tile_b.x);
              points.a = {
                x: wtx+terra/2,
                y: wty+terra/2
              }
              points.b = {
                x: points.a.x-terra,
                y: wty+terra/2
              }
            }
            break;
          }
        }
      }
    } else if (ty != undefined) {
      for (tx = 0; tx < tile_a.width; tx++) {
        let wtx = tile_a.x+(tx-tile_a.width/2)*terra;
        let wty = tile_a.y+(ty-tile_a.height/2)*terra;
     //   console.log(wtx, wty);
        if (intersects.lineBox(
          line.a.x, line.a.y, line.b.x, line.b.y,
          wtx, wty, terra, terra 
        )) {
          if (tile_a.grid[ty][tx] == 0 && tile_b.grid[ty][0] == 0) {
            if (tile_a.y < tile_b.y) { // Y++
            //  console.log("Y++", tile_a.y, tile_b.y);
              points.a = {
                x: wtx+terra/2,
                y: wty+terra/2
              }
              points.b = {
                x: wtx+terra/2,
                y: points.a.y+terra
              }
            } else if (tile_a.y > tile_b.y) { // Y--
            //  console.log("Y--", tile_a.y, tile_b.y);
              points.a = {
                x: wtx+terra/2,
                y: wty+terra/2
              }
              points.b = {
                x: wtx+terra/2,
                y: points.a.y-terra
              }
            }

            break;
          }
        }
      }
    } else {
      console.error(new Error("TILES DO NOT CONNECT"));
    }

    return points;
  }


  add_debug_mesh(x, z) {
    let geometry = new THREE.BoxGeometry(150, 10000, 150 );
    let material = new THREE.MeshBasicMaterial( {
      color: 0x00ff00,
      transparent: true,
      opacity: 0.3
    } );
    let cube = new THREE.Mesh( geometry, material );
    cube.position.set(x, 0, z);
    this.world.scene.add( cube );
  }

  debug_curterr() {
    let curterr = this.world.terrain.current_territory;
    let tw = this.world.terrain.width;
    for (let t = 0; t < this.tiles.length; t++) {
      let tile = this.tiles[t];
   //   console.log(tile.tile_x, curterr.tile_x-tw/2, tile.tile_y, curterr.tile_y-tw/2);
      if (tile.tile_x == curterr.tile_x-tw/2 &&
        tile.tile_y == curterr.tile_y-tw/2) {
        tile.updateGrid();
        tile.prepare_debug_mesh(); 
     //   console.log(tile.grid);
      }
    }
  }

  struct_tiles(obj) {
    for (let t = 0; t < this.tiles.length; t++) {
      let tile = this.tiles[t];
      if (tile.contains(obj.pos.x, obj.pos.z)) {
        return {
          tx: tile.tile_x, ty: tile.tile_y,
          list: tile.struct_tiles(obj)
        };
      }
    }
  }

  nearpoint(pos) {
    let ctx = this.world.terrain.current_territory.tile_x-this.width/2;
    let cty = this.world.terrain.current_territory.tile_y-this.height/2;

    for (let t = 0; t < this.tiles.length; t++) {
      let tile = this.tiles[t];
      //console.log(tile.tile_x, tile.tile_y);
      if (tile.tile_x == ctx && tile.tile_y == cty) {
        return tile.nearpoint(pos);
      }
    }
  }

  destroy() {
    for (let t = 0; t < this.tiles.length; t++) {
      this.tiles[t].destroy();
    }
  }
}
