
module.exports = class {
  static async init() {

  }

  constructor(x, y, width, height, world) {
    this.tile_x = x;
    this.tile_y = y;
    this.x = x*world.terrain.tile_size*50+world.terrain.tile_size*50/2;
    this.min_x = this.x-world.terrain.tile_size*50/2;
    this.max_x = this.x+world.terrain.tile_size*50/2;

    this.y = y*world.terrain.tile_size*50+world.terrain.tile_size*50/2;
    this.min_y = this.y-world.terrain.tile_size*50/2;
    this.max_y = this.y+world.terrain.tile_size*50/2;

    this.world = world;
    this.width = width;
    this.height = height;
    this.grid = [];


    this.debug = true;

    if (this.debug) {
      this.debug_meshes = [];
      this.debug_grid = []

      for (let h = 0; h < height; h++) {
        this.debug_grid[h] = [];
      }
    }
//   this.easystar.setGrid(this.grid);

    

    this.path_meshes = [];
  }

  contains(x, y) {
    if (
      this.min_x < x && x < this.max_x &&
      this.min_y < y && y < this.max_y
    ) {
      return true;
    } else {
      return false;
    }
  }

  struct_tiles(obj) {

//    console.log("STRUCT TILES <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    let terra = this.terra = 50/this.width*this.world.terrain.tile_size;
    let grid_tiles = [];
//    console.log(obj.aabb);
    for (let gx = obj.aabb.min_x; gx < obj.aabb.max_x; gx++) {
      for (let gy = obj.aabb.min_y; gy < obj.aabb.max_y; gy++) {
        let minX, minY, maxX, maxY;
        if (this.world.terrain.terropen) {
          minX = (gx-this.width/2)*terra;
          minY = (gy-this.height/2)*terra;
          maxX = (gx-this.width/2)*terra+terra;
          maxY = (gy-this.height/2)*terra+terra;
        } else {
          terra = 50/this.width*this.world.terrain.tile_size*50;
          minX = (gx-this.width/2)*terra;
          minY = (gy-this.height/2)*terra;
          maxX = (gx-this.width/2)*terra+terra;
          maxY = (gy-this.height/2)*terra+terra;
        }
        let aabb = {
          minX: minX,
          minY: minY,
          maxX: maxX,
          maxY: maxY,
          contains: function(cx, cy) {
            if (this.minX < cx && this.minY < cy && cx < this.maxX && cy < this.maxY) {
              return true;
            } else {
              return false
            }
          }
        }

        if (obj.aabb.collides(aabb, obj.p.x, obj.p.z)) {
          grid_tiles.push({ x: gx, y: gy});
        }
      }
    }
    return grid_tiles;
  }

  updateGrid() {
    let terra = this.terra = 50/this.width*this.world.terrain.tile_size;

    for (let h = 0; h < this.height; h++) {
      this.grid[h] = [];
      for (let w = 0; w < this.width; w++) {
        this.grid[h][w] = 0;
      }
    }

    let objects = this.world.structures.all();
    for (let o = 0; o < objects.length; o++) {
      let obj = objects[o];

      if (obj.grid_tiles) {
        if (obj.grid_tiles.tx == this.tile_x && obj.grid_tiles.ty == this.tile_y) {
          let list = obj.grid_tiles.list;
          for (let t = 0; t < list.length; t++) {
            let tile = list[t];
            this.grid[tile.y][tile.x] = 1;
          }
        }
      }

    }
 //   console.log("OBJSL", objects.length);

    if (!this.grid_tested) { 
      let this_class = this;
      setTimeout(function() {
        this_class.debug_output();
      }
      , 3000);
      this.grid_tested = true;
    }

  //  if (this.debug) this.prepare_debug_mesh();
    //console.log(this.grid);
  //  this.easystar.setGrid(this.grid);
  }

  async find_path(start, dest) {
    try {
      start = this.world.terrain.terr_coords(this.tile_x, this.tile_y, start.x, start.y);
      dest = this.world.terrain.terr_coords(this.tile_x, this.tile_y, dest.x, dest.y);
//      console.log("start", start);
//      console.log("dest", dest);
      start.x = Math.floor(start.x/this.terra+this.width/2);
      start.y = Math.floor(start.y/this.terra+this.height/2);
      dest.x = Math.floor(dest.x/this.terra+this.width/2);
      dest.y = Math.floor(dest.y/this.terra+this.height/2);
//      console.log("start", start);
//      console.log("dest", dest);
      let this_class = this;
      let easystar = this.world.astar.easystar;
      easystar.setGrid(this.grid);
      let result_path =  this.optimize_steps(await new Promise(function (resolve) {
        easystar.findPath(start.x, start.y, dest.x, dest.y, function( path ) {
          if (path === null) {
          //  alert("Path was not found.");
            console.warn("PATH WAS NOT FOUND");
          } else {
            console.log("Path was found.");
            resolve(path);
          }
        });
      }));
      
      if (this.debug) {
        this.draw_path(result_path);
      }
      for (let s = 0; s < result_path.length; s++) {
        let ux = (result_path[s].x-this.width/2)*this.terra+this.terra/2;
        let uy = (result_path[s].y-this.height/2)*this.terra+this.terra/2;
        let uvec = this.world.terrain.terr_to_world(new THREE.Vector3(ux, 0, uy), this.tile_x, this.tile_y);
        result_path[s] = { x: uvec.x, y: uvec.z };
      }

      return result_path;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  debug_output() {

  //  console.log("GRID >>>>>>>>>>>>>>>>>>>>>>>", this.grid);
    //console.log(this.world.structures.all());
 //   this.prepare_debug_mesh();
  }

  optimize_steps(path) {
    let optimized = [], last_step, last_dir;
    if ( path.length > 2 ) {
      path.splice(0, 1);
      path.splice(path.length-1, 1);
    }
    for (let p = 0; p < path.length; p++) {
      let step = path[p];
      if (last_step) {
        let dir = {
          x : step.x - last_step.x,
          y : step.y - last_step.y
        }

        if (last_dir && last_dir.x == dir.x && last_dir.y == dir.y) {
          optimized[optimized.length-1] = {
            x : step.x,
            y : step.y
          }
        } else {
          optimized.push({
            x : step.x,
            y : step.y
          });
        }

        last_dir = dir;
      } else {
        optimized.push({
          x : step.x,
          y : step.y
        });
      }

      last_step = step;
    }
    return optimized;
  }
 
  draw_path(path) {
    for (let m = 0; m < this.path_meshes.length; m++) {
      this.world.scene.remove(this.path_meshes[m]);
    }
    this.path_meshes = [];
    for (let p = 0; p < path.length; p++) {
      let step = path[p];
      let gx = step.x, gy = step.y;
//      console.log("step", gx, gy);
      let terra = 50/this.width*this.world.terrain.tile_size;
      let minX = (gx-this.width/2)*terra;
      let minY = (gy-this.height/2)*terra;
      let geometry = new THREE.BoxGeometry( terra, 10000, terra );
      let material = new THREE.MeshBasicMaterial( {
        color: 0x00ff00,
        transparent: true,
        opacity: 0.3
      } );
      let cube = new THREE.Mesh( geometry, material );
      cube.position.set(minX+terra/2, 0,minY+terra/2);
      this.world.scene.add( cube );
      this.path_meshes.push(cube);
    }
  }

  prepare_debug_mesh() {
    console.log("PREP DEBUG");
    let terra = this.terra = 50/this.width*this.world.terrain.tile_size;
    console.log("terra", terra);
    if (this.world.terrain.terropen && this.debug) {

      for (let gx = 0; gx < this.width; gx++) {
        for (let gy = 0; gy < this.height; gy++) {
          if ( this.grid[gy][gx] == 1 ) {
            let geometry = new THREE.BoxGeometry( terra, 10000, terra );
            let material = new THREE.MeshBasicMaterial( {
              color: 0xff0000,
              transparent: true,
              opacity: 0.1
            } );
            let cube = new THREE.Mesh( geometry, material );
            cube.position.set((gx-this.width/2)*terra+terra/2, 0, (gy-this.height/2)*terra+terra/2);
            this.debug_meshes.push(cube);
            this.world.scene.add( cube );
            this.debug_grid[gx][gy] = cube;
          }
        }
      }
    }
  }
  
  nearpoint(pos) {
    let terra = this.terra = 50/this.width*this.world.terrain.tile_size;
    let gx = Math.round(pos.x/this.terra+this.width/2);
    let gy = Math.round(pos.z/this.terra+this.height/2);
    //console.log(gy, gx, this.tile_x, this.tile_y);

    if (this.grid[gy][gx] != 1) {
      return pos;
    } else {
      let min_t = 4, max_t = 5;
      let npts = [];
      for (let tx = gx-min_t; tx < gx+max_t; tx++) {
        for (let ty = gy-min_t; ty < gy+max_t; ty++) {
          if (this.grid[ty][tx] != 1) {
            npts.push(new THREE.Vector3(
              (tx-this.width/2)*terra,
              0,
              (ty-this.height/2)*terra
            ));
          }
        }
      }
      npts.sort(function(a, b) {
        if (pos.clone().sub(a).length() > pos.clone().sub(b).length()) {
          return 1;
        } else {
          return -1;
        }
      });
      return npts[0];
    }
  }

  destroy() {
    for (let d = 0; d < this.debug_meshes.length; d++) {
      let debug_mesh = this.debug_meshes[d];
      this.world.scene.remove(debug_mesh);
    }
    for (let m = 0; m < this.path_meshes.length; m++) {
      this.world.scene.remove(this.path_meshes[m]);
    }
  }
}
