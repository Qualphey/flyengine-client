
const easystarjs = require('easystarjs');

const Grid = require("./Grid.js");

module.exports = class {
  constructor(width, height, world)  {
    this.grid = new Grid(width, height, world);
    this.easystar = new easystarjs.js();

    this.easystar.setAcceptableTiles([0]);
    this.easystar.enableDiagonals();
    this.easystar.enableCornerCutting();
    this.easystar.setIterationsPerCalculation(1000);
  }

  async find_path(start, dest) {
    try {
      return await this.grid.find_path(start, dest);
    } catch (e) {
      console.error(e.stack);
    }
  }

  debug_curterr() {
    this.grid.debug_curterr();
  }

  struct_tiles(obj) {
    return this.grid.struct_tiles(obj);
  }

  nearpoint(pos) {
    return this.grid.nearpoint(pos);
  }

  destroy() {
    this.grid.destroy();
  }
}
