
const WORLD = require("./World.js").default;

module.exports = class {
  constructor() {
  }

  async set_position(e) {
    let terrain = WORLD.terrain;
    let pos = await terrain.cast_ray(e);
    if (pos) {
      this.pos = pos;
      this.wpos = this.pos.clone();
      if (terrain.terropen) this.wpos = terrain.terr_to_world(this.pos.clone());
    }
  }

  intersect(mesh, e) {
    var mouse = new THREE.Vector2();
    mouse.x = ( e.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = - ( e.clientY / window.innerHeight ) * 2 + 1;

    var raycaster = new THREE.Raycaster();
    raycaster.setFromCamera( mouse, WORLD.camera );

    var intersects = raycaster.intersectObject( mesh );

    return intersects.length > 0 ? intersects : false;
  }
} 


