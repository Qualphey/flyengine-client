

module.exports = class {
  constructor() {
    this.list = [];

  }

  async load(list) {

    for (var m in list) {
      if (!this.list[m]) {
        this.list[m] = list[m];
        this.list[m].name = m;
      }
    }

    for (var m in this.list) {
      let model = this.list[m];
      model.name = m;

      if (!model.loaded) {
        let collada_loader;
        if (model.model_src.endsWith(".dae")) {
          collada_loader = new THREE.ColladaLoader();
        } else {
          collada_loader = new THREE.GLTFLoader();
        }
        console.log("LOADING MODEL", model.model_src);
        await new Promise(function(resolve) {
          collada_loader.load(model.model_src, function (collada) {

              console.log("mesh loaded", collada);
            var matrix4 = new THREE.Matrix4();

            matrix4.compose(
              new THREE.Vector3(0, 0, 0),
              new THREE.Quaternion().setFromAxisAngle( new THREE.Vector3( 1, 0, 0 ), -90*(Math.PI/180)),
              new THREE.Vector3(1, 1, 1)
            );

            if (collada.animations && 0 < collada.animations.length) {
//              collada.scene.children[1].geometry.applyMatrix(matrix4);
//            if (collada.scene.children.length == 2) {
              model.animations = collada.animations;
              model.mesh = collada.scene//.children[1].children[0];
            } else {
              if (model.model_src.endsWith(".dae")) {
                console.log(collada.scene);
                collada.scene.children[0].geometry.applyMatrix(matrix4);
                model.mesh = collada.scene.children[0];
              } else {
                model.mesh = collada.scene//.children[1].children[0];
              }
            }
              console.log("mesh loaded", collada);
            model.loaded = true;
            resolve();
          },
          // called while loading is progressing
          function ( xhr ) {

            console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );

          },
          // called when loading has errors
          function ( err ) {

            console.log( 'An error happened', err );

          });
        });

        var texture_loader = new THREE.TextureLoader();
        console.log("LOADING TEXTURE", model.texture_src);
        await new Promise(function(resolve) {
          texture_loader.load(model.texture_src, function(texture) {
            model.texture = texture;
            resolve();
          }, undefined, function ( err ) {
              // Function called when download errors
          //		console.log( 'An error happened' );
            console.log(err);
            resolve();
          });
        });
      }
    }

    var this_class = this;

    return await new Promise(function(resolve) {
      function ckStatus() {
        var loaded = true;
        for (var m in this_class.list) {
          var model = this_class.list[m];
          if (!model.loaded) {
            loaded = false;
          }
        }

        if (!loaded) {
          setTimeout(ckStatus, 333);
        } else {
          resolve();
        }
      }

      setTimeout(ckStatus, 333);
    });
  }

  destroy() {
    for (var m in this.list) {
      let model = this.list[m].mesh;
      console.log("DESTROY", model.name);
      model.geometry.dispose();
      if (model.material) model.material.dispose(); 
      model.texture.dispose();
    }
  }
}
