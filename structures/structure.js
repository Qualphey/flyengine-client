const GameObject = require("../GameObject.js");
const intersects = require("intersects");
const XHR = require('../utils/xhr.js');

const nunjucks = require('nunjucks');

/**
 * @class Structure
 * @extends GameObject
 * @hidecontructor
 */
module.exports = class extends GameObject {
  /**
   * @static
   * @async
   * @method init
   * @memberof Structure
   * @arg {World} world
   * @arg {Object} cfg
   */
  static async init(world, cfg) {
    try {
      let models = world.models.list;
      cfg.controls_html = await XHR.get("res/structures/"+cfg.name+"/controls.html");
      let this_class = new module.exports(models[cfg.name], world, cfg);
      await this_class.update();
      return this_class;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  constructor(model, world, cfg) {
    super(model, {
      x: cfg.x,
      z: cfg.y,
      angle: cfg.angle,
      scale: 20
    }, world);

/*
    this.addEventListener("mousemove", function(e) {
      console.log("struct_hover");
    });
    this.addEventListener("mousein", function(e) {
      console.log("mousein");
    });
    this.addEventListener("mouseout", function(e) {
      console.log("mouseout");
    });
  */  
    let cost = {
      wood: 10,
      stone: 20,
      iron: 5
    }
    console.log("THIS IS CFG: ", cfg);

    let this_class = this;
    this.addEventListener("click", async function(e) {
      try {
        if (e.button == 0) {
          this_class.select()
        } else if (e.button = 2) {
          if (this_class.types && this_class.types.includes('storage')) {
            for (let u = 0; u < world.player.selected_units.length; u++) {
              let nearpoint = world.astar.nearpoint(e.point);
              let wpoint = world.terrain.terr_to_world(nearpoint);
              let cunit = world.player.selected_units[u];
              let path = await cunit.find_path(nearpoint);
              world.player.socket.emit("world.ui-command", {
                command: "unit_store_res",
                path: path,
                x: wpoint.x,
                y: wpoint.z,
                id: world.player.selected_units[u].id,
                resources: cunit.inventory.all(),
                structure_id: e.obj.id,
                unit_id: cunit.id
              });
            }
          }
        }
      } catch (err) {
        console.error(err.stack);
      }
    });

    this.html_block = document.createElement("div");
    this.html_block.classList.add("structure_controls");
    const rendered_html = nunjucks.renderString(cfg.controls_html, {
      cost: 'W'+cost.wood+' S'+cost.stone+' I'+cost.iron
    });
    this.html_block.innerHTML = rendered_html;
    let xbtn = document.createElement("button");
    xbtn.innerHTML = "x";
    xbtn.classList.add("xbtn");
    xbtn.addEventListener("click", function(e) {
      this_class.deselect();
    });
    this.html_block.appendChild(xbtn);
    this.types = cfg.config.cfg.types || [];

    this.resources = {};


    if (!cfg.properties) cfg.properties = {}; 

    if (cfg.config.cfg.types && cfg.config.cfg.types.includes("storage")) {
      this.resource_table = this.html_block.querySelector("table");
      if (cfg.properties.resources) {
        for (let res_key in cfg.properties.resources) {
          let tr = document.createElement("tr");
          let td_type = document.createElement("td");
          td_type.innerHTML = res_key;
          tr.appendChild(td_type);
          let td_amount = document.createElement("td");
          td_amount.innerHTML = cfg.properties.resources[res_key];
          tr.appendChild(td_amount);
          this.resource_table.appendChild(tr);
          this.resources[res_key] = {
            td_amount: td_amount
          }
        }
      }
    }

    this.world = world;
    /**
      Name
      @name GameObject#name
      @type {String}
    */
    this.name = cfg.name;
    this.x = cfg.x;
    this.y = cfg.y;
    /**
      Id
      @name GameObject#id
      @type {String}
    */
    this.id = cfg.id;

    this.grid_tiles = cfg.grid_tiles;

   /* 
    if (models[cfg.name]) {
      let struct_model = models[cfg.name];
      let struct_mesh = this.struct_mesh = struct_model.mesh.clone();
      this.geometry = struct_mesh.geometry;
      this.material = struct_mesh.material = new THREE.MeshLambertMaterial({
        map: struct_model.texture,
      });
      this.texture = this.material.texture;
      struct_mesh.geometry.computeFaceNormals();
      struct_mesh.geometry.computeVertexNormals();

      let scale_val = 30;
      struct_mesh.scale.set(scale_val, scale_val, scale_val);
      let bbox = new THREE.Box3().setFromObject(struct_model.mesh.clone());
      let bpoly = [
        new THREE.Vector3(bbox.min.x, 0, bbox.min.z),
        new THREE.Vector3(bbox.max.x, 0, bbox.min.z),
        new THREE.Vector3(bbox.max.x, 0, bbox.max.z),
        new THREE.Vector3(bbox.min.x, 0, bbox.max.z)
      ]
      let bpoly_points = [];

      for (let p = 0; p < bpoly.length; p++) {
        let vertex = bpoly[p];
  
        let matrix4 = new THREE.Matrix4();
        matrix4.compose(
          vertex,
          new THREE.Quaternion().setFromAxisAngle( new THREE.Vector3( 0, 1, 0 ), cfg.angle ),
          new THREE.Vector3(scale_val, scale_val, scale_val)
        );
        vertex.applyMatrix4(matrix4);
        bpoly_points.push(vertex.x);
        bpoly_points.push(vertex.z);
      }

      this.aabb = {
        poly: bpoly_points,
        collides: function(aabb, tx, ty) {
          if (intersects.polygonBox(
            this.poly, aabb.minX-tx, aabb.minY-ty,
            aabb.maxX-aabb.minX, aabb.maxY-aabb.minY
          )) {
            return true;
          } else {
            return false;
          }
        }
      }
      
      struct_mesh.name = "structure_"+cfg.id;

      var pivot = this.pivot = new THREE.Group();
      pivot.add( struct_mesh );
      pivot.rotation.y = cfg.angle;

      //console.log("PIVOT ADDED");
    }*/
  }

  update_res(resources) {
    for (let res in resources) {
      if (this.resources[res]) {
        this.resources[res].td_amount.innerHTML = resources[res]
      } else {
        let tr = document.createElement("tr");
        let td_type = document.createElement("td");
        td_type.innerHTML = res;
        tr.appendChild(td_type);
        let td_amount = document.createElement("td");
        td_amount.innerHTML = resources[res]
        tr.appendChild(td_amount);
        this.resource_table.appendChild(tr);
        this.resources[res] = {
          td_amount: td_amount
        }
      }
    }
  }


  async update(delta) {
    try {
      super.update(delta);
      
      if (this.world.terrain.terropen && this.world.terrain.curterr_contains(this.x, this.y)) {
        if (!this.world.scene.getObjectByName(this.mname)) {
          let coords = this.world.terrain.curterr_coords(this.x, this.y);
          var raycaster = new THREE.Raycaster(new THREE.Vector3( coords.x, 10000, coords.y ), new THREE.Vector3( 0, -1, 0 ));
          var intersects = raycaster.intersectObject( this.world.terrain.cur_area[4].mesh );
          var distY = intersects[0].point.y;
          //console.log(distY);
          this.object.position.set( coords.x, distY, coords.y );

          this.world.scene.add(this.object);
          
        }
      } else {
        this.world.scene.remove(this.object);
        this.world.scene.remove(this.range_mesh);
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  screen_coords() {
    let obj = this.pivot;
    var vector = new THREE.Vector3();

    var widthHalf = 0.5*window.innerWidth;
    var heightHalf = 0.5*window.innerHeight;

    obj.updateMatrixWorld();
    vector.setFromMatrixPosition(obj.matrixWorld);
    vector.project(this.world.camera);

    vector.x = ( vector.x * widthHalf ) + widthHalf;
    vector.y = - ( vector.y * heightHalf ) + heightHalf;

    return { 
      x: vector.x,
      y: vector.y
    };

  }

  prep_range_geometry() {
    var MAX_POINTS = 2;

    // geometry
    var line_geometry = new THREE.BufferGeometry();

    // attributes
    var positions = new Float32Array( 361 * 3 ); // 3 vertices per point

    let coords = this.world.terrain.curterr_coords(this.x, this.y);
    
    let startpoint = new THREE.Vector3(coords.x, 0, coords.y), endpoint = new THREE.Vector3(coords.x-1200, 0, coords.y);

    let index = 0;

    for (let angle = 0; angle < 361; angle++) {
      let vector_delta = new THREE.Vector3().subVectors(endpoint, startpoint);
      vector_delta.applyAxisAngle( new THREE.Vector3(0, 1, 0),  angle*(Math.PI/180));
      let nvec = endpoint.clone().addVectors(startpoint, vector_delta); 

      let raycaster = new THREE.Raycaster(new THREE.Vector3( nvec.x, 10000, nvec.z ), new THREE.Vector3( 0, -1, 0 ));
      let intersects = raycaster.intersectObject( this.world.terrain.cur_area[4].mesh );
      let distY = intersects[0].point.y;

      positions[index++] = nvec.x;
      positions[index++] = distY+5;
      positions[index++] = nvec.z;

    }
/*

    positions[0] = endpoint.x;
    positions[1] = distY;
    positions[2] = endpoint.z;
    positions[3] = endpoint.x;
    positions[4] = 100000;
    positions[5] = endpoint.z;
i*/
    line_geometry.addAttribute( 'position', new THREE.BufferAttribute( positions, 3 ) );

    // draw range
    let drawCount = 361; // draw the first 2 points, only
    line_geometry.setDrawRange( 0, drawCount );

    // material
    var material = new THREE.LineBasicMaterial( {
      color: 0x00e433,
      linewidth: 10,
      transparent: true,
      opacity: 0.2
    });

    // line
    var line = new THREE.Line( line_geometry,  material );
    this.range_mesh = line;
    this.world.scene.add( line );
  }


  destroy() {
    this.world.scene.remove(this.range_mesh);
    super.destroy();
  }

  select() {
    this.selected = true;
    document.body.appendChild(this.html_block);
  }

  deselect() {
    this.selected = false;
    document.body.removeChild(this.html_block);
  }
}
