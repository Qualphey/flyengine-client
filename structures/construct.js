
const intersects = require("intersects");

module.exports = class {
  static async init(world, cfg) {
    try {
      return new module.exports(world, cfg);
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  constructor(world, cfg) {

    console.log("scfg", cfg);

    this.world = world;
    this.name = cfg.model;
    this.objs = [];

    this.node_nr = 0;

    this.angle = 0;
    this.debug_meshes = [];

    let models = world.models.list;
    
    if (models[cfg.model]) {
      let model = this.model = models[cfg.model]
      let mesh = model.mesh.clone();
      mesh.material = new THREE.MeshLambertMaterial({
        map: model.texture,
        transparent: true,
        opacity: 0.6
      });
      mesh.geometry.computeFaceNormals();
      mesh.geometry.computeVertexNormals();

      const scale_val = 30;
      mesh.scale.set(scale_val, scale_val, scale_val);
     
      var pivot = this.pivot = new THREE.Group();
      pivot.add( mesh ); 
      pivot.name = "construct";

      var raycaster = new THREE.Raycaster(new THREE.Vector3( 0, 10000, 0 ), new THREE.Vector3( 0, -1, 0 ));
      var intersects = raycaster.intersectObject( world.terrain.cur_area[4].mesh );
      var distY = intersects[0].point.y;
      console.log(distY);
      pivot.position.set( 0, distY, 0 );
      world.scene.add( pivot );
      console.log("PIVOT ADDED");
    }
  }

  position(pos) {
    if (!this.layout) {
      this.p = pos;
      this.pivot.position.set( pos.x, pos.y, pos.z );
      this.pos = pos;
    } else {
      let world = this.world;
      let rmpiv = world.scene.getObjectByName(this.pivot.name);
      world.scene.remove(rmpiv);

      for (let o = 0; o < this.objs.length; o++) {
        let rmobj = world.scene.getObjectByName(this.objs[o].name);
        world.scene.remove(rmobj);
      }

      this.objs = [];


      let dirvec = pos.sub(this.layout.start);

      let start_d = 0;
      if (this.layout.skip_first) start_d = 50;

      for (let d = start_d; d < dirvec.length(); d += 50) {
        let tvec = dirvec.clone().clampLength(0, d);
        let tmp_mesh = this.model.mesh.clone();
        tmp_mesh.material = new THREE.MeshLambertMaterial({
          map: this.model.texture,
          transparent: true,
          opacity: 0.6
        });
        tmp_mesh.geometry.computeFaceNormals();
        tmp_mesh.geometry.computeVertexNormals();

        const scale_val = 30;
        tmp_mesh.scale.set(scale_val, scale_val, scale_val);
       
        let tpos = this.layout.start.clone().add(tvec);

        let pivot = new THREE.Group(); 
        pivot.name = "construct_"+d;

        pivot.add( tmp_mesh );
        let raycaster = new THREE.Raycaster(new THREE.Vector3( tpos.x, 10000, tpos.z ), new THREE.Vector3( 0, -1, 0 ));
        let intersects = raycaster.intersectObject( world.terrain.cur_area[4].mesh );
        let distY = intersects[0].point.y;
        pivot.position.set( tpos.x, distY, tpos.z );

        let angle = Math.floor(Math.random() * 13) * 30 * (Math.PI/180);
        pivot.rotation.y = angle;
        pivot.tpos = tpos;
        pivot.angle = angle;

        this.objs.push(pivot);
        world.scene.add( pivot );


      }
    }
    this.pa_changed = true;
  }

  set_position(pos) {
    this.pos = pos;
    this.angle_input = true;
    this.pa_changed = true;
  }

  set_angle(angle) {
    this.angle = angle;
    this.pivot.rotation.y = angle;
    this.pa_changed = true;
  }

  update_grid_tiles() {
    let world = this.world;
    if (this.layout) {
      for (let o = 0; o < this.objs.length; o++) {
        let obj = this.objs[o];
        this.prep_bbox(obj.angle, obj);
        obj.grid_tiles = world.astar.struct_tiles(obj); 
      }
    } else {
      this.prep_bbox(this.angle);
      this.grid_tiles = world.astar.struct_tiles(this);    
    }
  }

  prep_bbox(angle, obj) {
    let world = this.world;
    let models = world.models.list;
    let scale_val = 15;
    
    let bbox = new THREE.Box3().setFromObject(models[this.name].mesh.clone());
    let bpoly = [
      new THREE.Vector3(bbox.min.x, 0, bbox.min.z),
      new THREE.Vector3(bbox.max.x, 0, bbox.min.z),
      new THREE.Vector3(bbox.max.x, 0, bbox.max.z),
      new THREE.Vector3(bbox.min.x, 0, bbox.max.z)
    ]
    let bpoly_points = [];

    let min_x = undefined;
    let max_x = undefined;
    let min_y = undefined;
    let max_y = undefined;

    for (let p = 0; p < bpoly.length; p++) {
      let vertex = bpoly[p];

      let matrix4 = new THREE.Matrix4();
      matrix4.compose(
        vertex,
        new THREE.Quaternion().setFromAxisAngle( new THREE.Vector3( 0, 1, 0 ), angle ),
        new THREE.Vector3(scale_val, scale_val, scale_val)
      );
      vertex.applyMatrix4(matrix4);
      bpoly_points.push(vertex.x);
      if (typeof min_x === "undefined" || vertex.x < min_x) {
        min_x = vertex.x;
      }
      if (typeof max_x === "undefined" || vertex.x > max_x) {
        max_x = vertex.x;
      }
      bpoly_points.push(vertex.z);
      if (typeof min_y === "undefined" || vertex.z < min_y) {
        min_y = vertex.z;
      }
      if (typeof max_y === "undefined" || vertex.z > max_y) {
        max_y = vertex.z;
      }
    }


    let terra = 50/800*this.world.terrain.tile_size;
    
    if (obj) {
      obj.aabb = {
        poly: bpoly_points,
        collides: function(aabb, tx, ty) {
          if (intersects.polygonBox(
            this.poly, aabb.minX-tx, aabb.minY-ty,
            aabb.maxX-aabb.minX, aabb.maxY-aabb.minY
          )) {
            return true;
          } else {
            return false;
          }
        }
      }
      obj.p = obj.tpos;
      obj.pos = this.world.terrain.terr_to_world(obj.tpos.clone());
    } else {
      this.aabb = {
        poly: bpoly_points,
        min_x: Math.round(800-((max_x-this.p.x)/terra+400)),
        min_y: Math.round(800-((max_y-this.p.z)/terra+400)),
        max_x: Math.round(800-((min_x-this.p.x)/terra+400)),
        max_y: Math.round(800-((min_y-this.p.z)/terra+400)),
        collides: function(aabb, tx, ty) {
          if (intersects.polygonBox(
            this.poly, aabb.minX-tx, aabb.minY-ty,
            aabb.maxX-aabb.minX, aabb.maxY-aabb.minY
          )) {
            return true;
          } else {
            return false;
          }
        }
      }
    }
  }

  lay_out(start) {
    let world = this.world;
    console.log("LAYOUT");
    this.layout = {
      start: start
    }
    if (this.node_nr > 0) {
      this.layout.skip_first = true;
    }
    this.node_nr++;
  }

  layout_data() {
    if (this.layout) {
      let data = [];

      for (let o = 0; o < this.objs.length; o++) {
        let obj = this.objs[o];
        data.push({
          what: this.name,
          pos: obj.pos,
          angle: obj.angle,
          grid_tiles: obj.grid_tiles
        });
      }

      return data;
    } else {
      console.error("NO LAYOUT!");
      return undefined;
    }
  }
  
  display_tiles() {
    for (let d = 0; d < this.debug_meshes.length; d++) {
      this.world.scene.remove(this.debug_meshes[d]);
    }
    for (let t = 0; t < this.grid_tiles.list.length; t++) {
      let tile = this.grid_tiles.list[t];
      let terra = 50/800*this.world.terrain.tile_size;
      let geometry = new THREE.BoxGeometry( terra, 10000, terra );
      let material = new THREE.MeshBasicMaterial( {
        color: 0xff0000,
        transparent: true,
        opacity: 0.1
      } );
      let cube = new THREE.Mesh( geometry, material );
      cube.position.set((tile.x-800/2)*terra+terra/2, 0, (tile.y-800/2)*terra+terra/2);
      this.debug_meshes.push(cube);
      this.world.scene.add( cube );
   //   this.debug_grid[gx][gy] = cube;
    }
  }

  update() {
    let mpos = this.world.mouse.pos.clone();
    if (this.angle_input && !this.layout) {
      let point = mpos;
      let angle = Math.atan2(
        point.x - this.p.x,
        point.z - this.p.z
      );
      this.set_angle(angle);
    } else {
      this.position(mpos);
    }

    if (this.pa_changed) {
      this.update_grid_tiles();

      this.display_tiles();
      
      this.pa_changed = false;
    }
  }

  cancel() {
    let world = this.world;
    for (let o = 0; o < this.objs.length; o++) {
      let rmobj = world.scene.getObjectByName(this.objs[o].name);
      world.scene.remove(rmobj);
    }

    let rmpiv = world.scene.getObjectByName(this.pivot.name);
    world.scene.remove(rmpiv);

    this.objs = [];
    this.layout = undefined;
  }
}
