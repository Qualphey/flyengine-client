
const intersects = require("intersects");
const GameObject = require("../GameObject.js");

module.exports = class extends GameObject {
  static async init(world, cfg) {
    try {
      cfg.z = cfg.y;
      cfg.scale = 20;
      cfg.dont_add = false; // true not to add any meshes to the scene
      return new module.exports(world, cfg);
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  constructor(world, cfg) {
    super(world.models.list["construction"], cfg, world);
    console.log("CONSTRUCTION:", cfg);
    
    this.type = "construction";
    this.world = world;
    this.name = cfg.name;

    this.id = cfg.id;
    this.res_available = cfg.res_available;
    this.res_used = cfg.res_used;

    this.grid_tiles = cfg.grid_tiles;

    let construct_opt = this.opt_div = document.createElement("div");
    construct_opt.classList.add("construct_opt");

    let this_class = this;

    let build_btn = document.createElement("button");
    build_btn.innerHTML = "build";
    build_btn.addEventListener("click", async function () {
      for (let u = 0;u < this_class.world.player.selected_units.length; u++) {
        let nearpoint = this_class.world.astar.nearpoint(this_class.cur_click);
        let wpoint = this_class.world.terrain.terr_to_world(nearpoint);
        let cunit = this_class.world.player.selected_units[u];
        let path = await cunit.find_path(nearpoint);
        this_class.world.player.socket.emit("world.ui-command", {
          command: "unit_construct",
          path: path,
          x: wpoint.x,
          y: wpoint.z,
          id: world.player.selected_units[u].id,
          construction_id: this_class.id
        });
      }
    });
    construct_opt.appendChild(build_btn);

    let res_btn = document.createElement("button");
    res_btn.innerHTML = "res";
    res_btn.addEventListener("click", async function () {
      for (let u = 0;u < this_class.world.player.selected_units.length; u++) {
        let nearpoint = this_class.world.astar.nearpoint(this_class.cur_click);
        let wpoint = this_class.world.terrain.terr_to_world(nearpoint);
        let cunit = this_class.world.player.selected_units[u];
        let path = await cunit.find_path(nearpoint);
        this_class.world.player.socket.emit("world.ui-command", {
          command: "unit_bringres",
          path: path,
          x: wpoint.x,
          y: wpoint.z,
          id: world.player.selected_units[u].id,
          construction_id: this_class.id
        });
      }
    });
    construct_opt.appendChild(res_btn);
    
    let destroy_btn = document.createElement("button");
    destroy_btn.innerHTML = "destroy";
    destroy_btn.addEventListener("click", async function () {
      this_class.world.player.socket.emit("world.ui-command", {
        command: "destroy",
        type: "construction",
        construction_id: this_class.id
      });
    });
    construct_opt.appendChild(destroy_btn);

    this.addEventListener("click", function(e) {
      if (world.player.selected_units.length > 0 && e.button == 2) {
        let hit = world.structures.mouse(e);
        if (hit && hit.obj.type == "construction") {
          this_class.cur_click = hit.point;
          if (!document.body.contains(this_class.opt_div)) {
            document.body.appendChild(this_class.opt_div);
            world.opt_div = this_class.opt_div;
          }
        }
      }
    });
/*
    for (let i in cfg.res_available) {
      construct_opt.innerHTML += " "+i[0]+cfg.res_available[i];
    }
*/

    let models = world.models.list;
    if (models[cfg.name]) {
      let struct_model = models[cfg.name]
      let struct_mesh = this.struct_mesh = struct_model.mesh.clone();
      struct_mesh.material = new THREE.MeshLambertMaterial({
        map: struct_model.texture,
      });
      struct_mesh.geometry.computeFaceNormals();
      struct_mesh.geometry.computeVertexNormals();

      let scale_val = 1;

      struct_mesh.scale.set(scale_val, scale_val, scale_val);
      
      let struct_height = this.struct_height = (new THREE.Box3().setFromObject(struct_mesh)).getSize().y;

      let struct_config = this.struct_config = this.world.structures.config[cfg.name].lvls[cfg.lvl]; 
      let total_cost = 0;
      for (let c in struct_config.cost) {
        total_cost += struct_config.cost[c];
      }
      let total_used = 0;
      for (let c in this.res_used) {
        total_used += this.res_used[c];
      }

      let build_height = struct_height*total_used/total_cost;

      let struct_y = build_height-struct_height;

      struct_mesh.position.set(0, struct_y, 0);
      
      struct_mesh.name = "structure_"+cfg.id;

      this.object.add( struct_mesh );
    }
  }

  progress(avail, used) {
    this.res_available = avail;
    this.res_used = used;

    let total_cost = 0;
    for (let c in this.struct_config.cost) {
      total_cost += this.struct_config.cost[c];
    }
    let total_used = 0;
    for (let c in this.res_used) {
      total_used += this.res_used[c];
    }

    let build_height = this.struct_height*total_used/total_cost;

    let struct_y = build_height-this.struct_height;

    this.struct_mesh.position.set(0, struct_y, 0);

  }

  async update(delta) {
    try {
      super.update(delta);
      if (this.world.terrain.terropen && this.world.terrain.curterr_contains(this.x, this.z)) {
        if (!this.world.scene.getObjectByName(this.object.name)) {
          let coords = this.world.terrain.curterr_coords(this.x, this.z);
          var raycaster = new THREE.Raycaster(new THREE.Vector3( coords.x, 10000, coords.y ), new THREE.Vector3( 0, -1, 0 ));
          var intersects = raycaster.intersectObject( this.world.terrain.cur_area[4].mesh );
          var distY = intersects[0].point.y;
          console.log(coords.x, distY, coords.y);
          this.object.position.set( coords.x, distY, coords.y );

          this.world.scene.add(this.object);
          
        }
      } else {
        if (this.world.scene.getObjectByName(this.object.name)) {
          this.world.scene.remove(this.object);
        }
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  screen_coords() {
    let obj = this.object;
    var vector = new THREE.Vector3();

    var widthHalf = 0.5*window.innerWidth;
    var heightHalf = 0.5*window.innerHeight;

    obj.updateMatrixWorld();
    vector.setFromMatrixPosition(obj.matrixWorld);
    vector.project(this.world.camera);

    vector.x = ( vector.x * widthHalf ) + widthHalf;
    vector.y = - ( vector.y * heightHalf ) + heightHalf;

    return { 
      x: vector.x,
      y: vector.y
    };

  }


  render() {

    let screen_coords = this.screen_coords();
    this.opt_div.style.left = screen_coords.x+"px";
    this.opt_div.style.top = screen_coords.y+"px"
  }

  remove() {
    this.world.scene.remove(this.object);
  }
}
