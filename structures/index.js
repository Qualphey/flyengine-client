
const Construct = require("./construct.js");
const Construction = require("./construction.js");
const Structure = require("./structure.js");

const all_structures = [
  "stockpile", "wall"
]

/**
 * Class handling all structures
 * @class Structures
 * @hideconstructor
 */
module.exports = class {
  static async init(world) {
    try {
      return new module.exports(world);
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  constructor(world) {
    this.world = world;
    this.list = [];
    this.constructions = [];
    this.menu = [];
    
    for (let s = 0; s < all_structures.length; s++) {
      let sname = all_structures[s];
      let nstruct = {
        model: sname,
        menu_img: "res/structures/"+sname+"/lvl0-menu.png",
        controls_html: "res/structures/"+sname+"/controls.html"
      }
      this.menu.push(nstruct);
    }
  }

  select_construction(id) {
    for (let c = 0; c < this.constructions.length; c++) {
      if (this.constructions[c].id == id) {
        return this.constructions[c];
      }
    }
  }

  select_structure(id) {
    for (let c = 0; c < this.list.length; c++) {
      if (this.list[c].id == id) {
        return this.list[c];
      }
    }
  }

  mouse(e) {
    let mouse = new THREE.Vector2();
    mouse.x = ( e.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = - ( e.clientY / window.innerHeight ) * 2 + 1;
    for (let t = 0; t < this.constructions.length; t++) {
      let raycaster = new THREE.Raycaster();
      raycaster.setFromCamera( mouse, this.world.camera );

      let intersects = raycaster.intersectObject( this.constructions[t].mesh );
      
      if (intersects[0]) {
        return {
          obj: this.constructions[t],
          point: new THREE.Vector3(intersects[0].point.x, intersects[0].point.y, intersects[0].point.z)
        };
      }
    }
    for (let t = 0; t < this.list.length; t++) {
      let raycaster = new THREE.Raycaster();
      raycaster.setFromCamera( mouse, this.world.camera );

      let intersects = raycaster.intersectObject( this.list[t].mesh );
      
      if (intersects[0]) {
        return {
          obj: this.list[t],
          point: new THREE.Vector3(intersects[0].point.x, intersects[0].point.y, intersects[0].point.z)
        };
      }
    }
  }

  async build(cfg) {
    try { 
      return await Construct.init(this.world, cfg); 
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }
  
  async set_config(cfg) {
    this.config = cfg;
  }

  async add_construction(cfg) {
    this.constructions.push(await Construction.init(this.world, cfg));
  }

  remove_construction(id) {
    for (let i = 0; i < this.constructions.length; i++) {
      if (this.constructions[i].id == id) {
        this.constructions[i].remove();
        this.constructions.splice(i, 1);
        break;
      }
    }
  }

  async add_structure(cfg) {
    this.list.push(await Structure.init(this.world, cfg));
  }

  prep_geometries() {
    for (let c = 0; c < this.list.length; c++) {
      if (this.list[c].name == "stockpile") {
        this.list[c].prep_range_geometry();
      }
    }
  }

  update(delta) {
    for (let c = 0; c < this.constructions.length; c++) {
      this.constructions[c].update(delta);
    }
    for (let c = 0; c < this.list.length; c++) {
      this.list[c].update(delta);
    }
  }

  render() {
    for (let c = 0; c < this.constructions.length; c++) {
      if (this.constructions[c]) {
        this.constructions[c].render();
      }
    }
/*    for (let c = 0; c < this.list.length; c++) {
      if (this.list[c]) {
        this.list[c].render();
      }
    }*/
  }

  all() {
    let all_structs = [];
    for (let c = 0; c < this.constructions.length; c++) {
      all_structs.push(this.constructions[c]);
    }
    for (let c = 0; c < this.list.length; c++) {
      all_structs.push(this.list[c]);
    } 

    console.log("CONSTRUCT", this.world.player.construct);

    if (this.world.player.construct) {
      all_structs.push(this.world.player.construct); 
    }

    return all_structs;
  }


  destroy() {
    for (let c = 0; c < this.constructions.length; c++) {
      this.constructions[c].destroy();
    }
    for (let c = 0; c < this.list.length; c++) {
      this.list[c].destroy();
    }
  }
}
